const argv = require("./argv")
const fs = require("fs")
const dotenv = require("dotenv")
const dotenvSafe = require("dotenv-safe")

if (fs.existsSync(".env")) {
    dotenv.config()
    dotenvSafe.config({
        allowEmptyValues: true,
    })
}

module.exports = parseInt(argv.port || process.env.PORT || "3000", 10)
