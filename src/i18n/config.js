import { language } from "../config"

const config = {
    defaultLanguage: language,
    options: [
        {
            languageId: "english",
            locale: "en",
            text: "English",
            icon: import("styles/images/flag/uk.svg"),
        },
        {
            languageId: "chinese",
            locale: "zh",
            text: "Chinese",
            icon: import("styles/images/flag/china.svg"),
        },
        {
            languageId: "spanish",
            locale: "es",
            text: "Spanish",
            icon: import("styles/images/flag/spain.svg"),
        },
        {
            languageId: "french",
            locale: "fr",
            text: "French",
            icon: import("styles/images/flag/france.svg"),
        },
        {
            languageId: "italian",
            locale: "it",
            text: "Italian",
            icon: import("styles/images/flag/italy.svg"),
        },
    ],
}

export function getCurrentLanguage(lang) {
    let selecetedLanguage = config.options[0]
    config.options.forEach((item) => {
        if (item.languageId === lang) {
            selecetedLanguage = item
        }
    })
    return selecetedLanguage
}
export default config
