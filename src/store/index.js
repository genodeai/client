import { createStore, combineReducers, applyMiddleware } from "redux"
import createHistory from "history/createBrowserHistory"
import { routerReducer, routerMiddleware } from "react-router-redux"
import createSagaMiddleware from "redux-saga"
import { composeWithDevTools } from "redux-devtools-extension"

import { isProductionEnv } from "helpers/utility"

import reducers from "./reducers"
import rootSaga from "./sagas"

const history = createHistory()
const sagaMiddleware = createSagaMiddleware()
const routeMiddleware = routerMiddleware(history)
const middlewares = [sagaMiddleware, routeMiddleware]

const store = createStore(
    combineReducers({
        ...reducers,
        router: routerReducer,
    }),
    isProductionEnv() === true
        ? applyMiddleware(...middlewares)
        : composeWithDevTools(applyMiddleware(...middlewares))
)
sagaMiddleware.run(rootSaga)

export {
    store,
    history,
}
