import app from "./app/reducer"
import auth from "./auth/reducer"
import vehicles from "./vehicles/reducer"

export default {
    app, auth, vehicles,
}
