import request from "helpers/request"

const BASE = "/api/v1/login"

export const login = params => async () => (
    await request("POST", `${BASE}/email`, params, false)
).data

export const loginByToken = () => async () => (
    await request("POST", `${BASE}/token`)
).data

export const forgotPassword = params => async () => (
    await request("POST", `${BASE}/resetPassword`, params, false)
).data

export const resetPasswordConfirm = params => async () => (
    await request("POST", `${BASE}/confirmResetPassword`, params, false)
).data

export const resetPasswordValidateToken = params => async () => (
    await request("POST", `${BASE}/isValidResetPasswordToken`, params, false)
).data

export const confirmEmailValidateToken = params => async () => (
    await request("POST", `${BASE}/confirmEmail`, params, false)
).data
