const a = {
    API__AUTH__LOGIN__REQUEST: "API__AUTH__LOGIN__REQUEST",
    API__AUTH__LOGIN__SUCCESS: "API__AUTH__LOGIN__SUCCESS",
    API__AUTH__LOGIN__ERROR: "API__AUTH__LOGIN__ERROR",

    API__AUTH__LOGIN_BY_TOKEN__REQUEST: "API__AUTH__LOGIN_BY_TOKEN__REQUEST",
    API__AUTH__LOGIN_BY_TOKEN__ERROR: "API__AUTH__LOGIN_BY_TOKEN__ERROR",

    API__AUTH__FORGOT_PASSWORD__REQUEST: "API__AUTH__FORGOT_PASSWORD__REQUEST",
    API__AUTH__FORGOT_PASSWORD__SUCCESS: "API__AUTH__FORGOT_PASSWORD__SUCCESS",
    API__AUTH__FORGOT_PASSWORD__ERROR: "API__AUTH__FORGOT_PASSWORD__ERROR",

    API__AUTH__RESET_PASSWORD__CONFIRM__REQUEST: "API__AUTH__RESET_PASSWORD__CONFIRM__REQUEST",
    API__AUTH__RESET_PASSWORD__CONFIRM__SUCCESS: "API__AUTH__RESET_PASSWORD__CONFIRM__SUCCESS",
    API__AUTH__RESET_PASSWORD__CONFIRM__ERROR: "API__AUTH__RESET_PASSWORD__CONFIRM__ERROR",

    API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__REQUEST: "API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__REQUEST",
    API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__SUCCESS: "API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__SUCCESS",
    API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__ERROR: "API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__ERROR",

    API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__REQUEST: "API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__REQUEST",
    API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__SUCCESS: "API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__SUCCESS",
    API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__ERROR: "API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__ERROR",

    LOCAL__AUTH__LOGOUT: "LOCAL__AUTH__LOGOUT",
    LOCAL__AUTH__CLEAR_STATE: "LOCAL__AUTH__CLEAR_STATE",

    requestLogin: (data, remember) => ({ type: a.API__AUTH__LOGIN__REQUEST, payload: { data, remember } }),
    successLogin: payload => ({ type: a.API__AUTH__LOGIN__SUCCESS, payload }),
    errorLogin: payload => ({ type: a.API__AUTH__LOGIN__ERROR, payload }),

    requestLoginByToken: () => ({ type: a.API__AUTH__LOGIN_BY_TOKEN__REQUEST }),
    errorLoginByToken: () => ({ type: a.API__AUTH__LOGIN_BY_TOKEN__ERROR, payload: {} }),

    requestForgotPassword: payload => ({ type: a.API__AUTH__FORGOT_PASSWORD__REQUEST, payload }),
    successForgotPassword: payload => ({ type: a.API__AUTH__FORGOT_PASSWORD__SUCCESS, payload }),
    errorForgotPassword: payload => ({ type: a.API__AUTH__FORGOT_PASSWORD__ERROR, payload }),

    requestResetPasswordConfirm: payload => ({ type: a.API__AUTH__RESET_PASSWORD__CONFIRM__REQUEST, payload }),
    successResetPasswordConfirm: payload => ({ type: a.API__AUTH__RESET_PASSWORD__CONFIRM__SUCCESS, payload }),
    errorResetPasswordConfirm: payload => ({ type: a.API__AUTH__RESET_PASSWORD__CONFIRM__ERROR, payload }),

    requestResetPasswordValidateToken: token => ({
        type: a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__REQUEST, payload: { token },
    }),
    successResetPasswordValidateToken: () => ({
        type: a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__SUCCESS,
    }),
    errorResetPasswordValidateToken: payload => ({
        type: a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__ERROR, payload,
    }),
    requestConfirmEmailValidateToken: token => ({
        type: a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__REQUEST, payload: { token },
    }),
    successConfirmEmailValidateToken: () => ({
        type: a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__SUCCESS,
    }),
    errorConfirmEmailValidateToken: payload => ({
        type: a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__ERROR, payload,
    }),

    logout: () => ({ type: a.LOCAL__AUTH__LOGOUT }),
    clearState: () => ({ type: a.LOCAL__AUTH__CLEAR_STATE }),

}
export default a
