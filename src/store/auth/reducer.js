import { assign } from "lodash/fp"

import a from "./actions"
import aUser from "../user/actions"

const initialState = {
    fetching: false,
    authenticated: false,
    isTokenValid: false,
    success: false,
    errors: {},
}

export default function (state = initialState, action) {
    switch (action.type) {
        case a.API__AUTH__LOGIN__REQUEST:
        case a.API__AUTH__LOGIN_BY_TOKEN__REQUEST:
        case a.API__AUTH__FORGOT_PASSWORD__REQUEST:
        case a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__REQUEST:
        case a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__REQUEST:
        case a.API__AUTH__REGISTER__REQUEST:
        case aUser.API__USER__INVITE_CONFIRM__REQUEST:
            return assign(initialState, { fetching: true })

        case a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__SUCCESS:
        case a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__SUCCESS:
            return assign(initialState, { isTokenValid: true })

        case a.API__AUTH__RESET_PASSWORD__CONFIRM__SUCCESS:
        case aUser.API__USER__INVITE_CONFIRM__SUCCESS:
            return assign(state, { success: true })

        case a.API__AUTH__REGISTER__SUCCESS:
        case a.API__AUTH__FORGOT_PASSWORD__SUCCESS:
            return assign(initialState, { success: true })

        case a.API__AUTH__LOGIN__SUCCESS:
            return assign(initialState, { authenticated: true })

        case a.API__AUTH__LOGIN__ERROR:
        case a.API__AUTH__LOGIN_BY_TOKEN__ERROR:
        case a.API__AUTH__FORGOT_PASSWORD__ERROR:
        case a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__ERROR:
        case a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__ERROR:
        case a.API__AUTH__REGISTER__ERROR:
            return assign(initialState, { errors: action.payload })

        case a.LOCAL__AUTH__LOGOUT:
        case a.LOCAL__AUTH__CLEAR_STATE:
            return initialState

        default:
            return state
    }
}
