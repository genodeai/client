import { takeEvery, put, call, select } from "redux-saga/effects"
import { push } from "react-router-redux"

import Token from "helpers/token"
import { PUBLIC_ROUTES } from "helpers/constants"
import { getLocation } from "store/router/selector"
import aMerchant from "store/merchant/actions"
import aLocation from "store/location/actions"
import aLocationDefault from "store/location-default/actions"
import aCohort from "store/cohort/actions"
import aCampaigns from "store/campaign/actions"
import aUser from "store/user/actions"
import aRole from "store/role/actions"
import { getByUUID } from "store/user/api"

import a from "./actions"
import * as api from "./api"

function* handleLoginSuccess(payload, data) {
    const { uuid, token } = data

    Token.set(token, payload.remember)
    const user = yield call(getByUUID(uuid))

    yield put(a.successLogin(user))

    const location = yield select(getLocation)
    if (user.merchantId) {
        yield put(aMerchant.requestGet())
        yield put(aLocation.requestGet())
        yield put(aLocationDefault.requestGet())
        yield put(aCohort.requestGet())
        yield put(aCohort.requestGetFeature())
        yield put(aCohort.requestGetDictionary())
        yield put(aCampaigns.requestGet())
        yield put(aUser.requestListByMerchant())
    }
    yield put(aUser.requestPermissions())
    yield put(aRole.requestGet())

    if (PUBLIC_ROUTES.includes(location.pathname) === true) {
        yield put(push(!user.merchantId ? "/dashboard/merchant" : "/dashboard"))
    } else {
        yield put(push(location.pathname))
    }
}
function* handleLogin({ payload }) {
    try {
        const result = yield call(api.login(payload.data))
        yield handleLoginSuccess(payload, result)
    } catch (e) {
        yield put(a.errorLogin(e.response.data))
    }
}
function* handleLoginByToken() {
    try {
        const tokenObj = Token.get()
        if (tokenObj === null) {
            yield put(a.errorLoginByToken())
        } else {
            const result = yield call(api.loginByToken())
            yield handleLoginSuccess(tokenObj, result)
        }
    } catch (e) {
        Token.clear()
        yield put(push("/signin"))
        yield put(a.errorLoginByToken())
    }
}

function* handleForgotPassword({ payload }) {
    try {
        yield call(api.forgotPassword({ email: payload.email }))
        yield put(a.successForgotPassword())
    } catch (e) {
        yield put(a.errorForgotPassword(e.response.data))
    }
}

function* handleResetPasswordConfirm({ payload }) {
    try {
        yield call(api.resetPasswordConfirm(payload))
        yield put(a.successResetPasswordConfirm())
    } catch (e) {
        yield put(a.errorResetPasswordConfirm(e.response.data))
    }
}
function* handleResetPasswordValidateToken({ payload }) {
    try {
        yield call(api.resetPasswordValidateToken(payload))
        yield put(a.successResetPasswordValidateToken())
    } catch (e) {
        yield put(a.errorResetPasswordValidateToken(e.response.data))
    }
}

function* handleConfirmEmailValidateToken({ payload }) {
    try {
        yield call(api.confirmEmailValidateToken(payload))
        yield put(a.successConfirmEmailValidateToken())
    } catch (e) {
        yield put(a.errorConfirmEmailValidateToken(e.response.data))
    }
}

function* handleLogout() {
    Token.clear()
    yield put(push("/signin"))
}

export default function* watcherUser() {
    yield takeEvery(a.API__AUTH__LOGIN__REQUEST, handleLogin)
    yield takeEvery(a.API__AUTH__LOGIN_BY_TOKEN__REQUEST, handleLoginByToken)
    yield takeEvery(a.API__AUTH__FORGOT_PASSWORD__REQUEST, handleForgotPassword)
    yield takeEvery(a.API__AUTH__RESET_PASSWORD__CONFIRM__REQUEST, handleResetPasswordConfirm)
    yield takeEvery(a.API__AUTH__RESET_PASSWORD__VALIDATE_TOKEN__REQUEST, handleResetPasswordValidateToken)
    yield takeEvery(a.API__AUTH__CONFIRM_EMAIL__VALIDATE_TOKEN__REQUEST, handleConfirmEmailValidateToken)
    yield takeEvery(a.LOCAL__AUTH__LOGOUT, handleLogout)
}
