export const getMerchantId = state => state.user.info.data.merchantId
export const getUUID = state => state.user.info.data.uuid
export const getRole = state => state.user.info.data.role
