import request from "helpers/request"
import { select } from "redux-saga/effects"
import { stringify } from "querystring"

import { getUUID } from "./selector"

const BASE = "/users/api/v1"
const BASE_USERS = `${BASE}/users`
const BASE_ACCOUNT = `${BASE}/account`

function* getUrl(path = "") {
    const uuid = yield select(getUUID)
    return `${BASE_USERS}/${uuid}${path}`
}

export const create = params => function* () {
    return (yield request("POST", BASE_USERS, params, false)).data
}

export const getByUUID = uuid => function* () {
    return (yield request("GET", `${BASE_USERS}/${uuid}`)).data
}

export const getWithFilter = filter => function* () {
    const query = stringify({
        filter: JSON.stringify(filter),
    })
    return (yield request("GET", `${BASE_USERS}?${query}`)).data
}

export const update = params => function* () {
    return (yield request("PATCH", yield getUrl(), params)).data
}

export const invite = params => function* () {
    return (yield request("POST", `${BASE_ACCOUNT}/invites`, params)).data
}

export const inviteConfirm = (token, params) => function* () {
    return (yield request("POST", `${BASE_ACCOUNT}/invites/${token}/confirm`, params)).data
}
