import { takeEvery, put, call, select } from "redux-saga/effects"

import * as apiRole from "store/role/api"
import aApp from "store/app/actions"

import a from "./actions"
import * as api from "./api"
import { getRole, getMerchantId } from "./selector"

function* handleCreate({ payload }) {
    try {
        const user = yield call(api.create(payload))
        yield put(a.successCreate({ email: user.email }))
    } catch (e) {
        yield put(a.errorCreate({ message: e.response.data.message }))
    }
}

function* handleUpdate({ payload }) {
    try {
        const user = yield call(api.update(payload))
        yield put(a.successUpdate(user))
    } catch (e) {
        yield put(a.errorUpdate({ message: e.response.data.message }))
    }
}

function* handleGetPermissions({ payload }) {
    try {
        let { role } = payload
        if (!role) {
            role = yield select(getRole)
        }
        if (role) {
            const { permissions } = (yield call(apiRole.getByUUID(role)))
            yield put(a.successPermissions(permissions))
        }
    } catch (e) {
        yield put(a.errorPermissions())
    }
}

function* handleListByMerchant() {
    try {
        const merchantId = yield select(getMerchantId)
        const users = yield call(api.getWithFilter({ merchantId }))
        yield put(a.successListByMerchant(users))
    } catch (e) {
        yield put(a.errorListByMerchant())
    }
}

function* handleInvite({ payload }) {
    try {
        const user = yield call(api.invite(payload))
        yield put(a.successInvite(user))
        yield put(aApp.pushNotificationWithSuccess(
            "user.invite.success.message",
            "user.invite.success.description",
        ))
        yield (put(aApp.changeCurrent(`/dashboard/users/${user.uuid}`, true)))
    } catch ({ response: r }) {
        yield put(a.errorInvite())
        yield put(aApp.pushNotificationWithError(r.data.error, r.data.message))
    }
}

function* handleInviteConfirm({ payload }) {
    try {
        const user = yield call(api.inviteConfirm(payload.token, payload.data))
        yield put(a.successInviteConfirm(user))
    } catch ({ response: r }) {
        yield put(a.errorInviteConfirm())
    }
}


export default function* watcherUser() {
    yield takeEvery(a.API__USER__CREATE__REQUEST, handleCreate)
    yield takeEvery(a.API__USER__UPDATE__REQUEST, handleUpdate)
    yield takeEvery(a.API__USER__INVITE__REQUEST, handleInvite)
    yield takeEvery(a.API__USER__INVITE_CONFIRM__REQUEST, handleInviteConfirm)
    yield takeEvery(a.API__USER__GET__PERMISSIONS__REQUEST, handleGetPermissions)
    yield takeEvery(a.API__USER__LIST_BY_MERCHANT__REQUEST, handleListByMerchant)
}
