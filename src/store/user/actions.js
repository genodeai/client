import { createAction } from "helpers/utility"

const c = {
    API__USER__CREATE__REQUEST: "API__USER__CREATE__REQUEST",
    API__USER__CREATE__SUCCESS: "API__USER__CREATE__SUCCESS",
    API__USER__CREATE__ERROR: "API__USER__CREATE__ERROR",

    API__USER__UPDATE__REQUEST: "API__USER__UPDATE__REQUEST",
    API__USER__UPDATE__SUCCESS: "API__USER__UPDATE__SUCCESS",
    API__USER__UPDATE__ERROR: "API__USER__UPDATE__ERROR",

    API__USER__GET__PERMISSIONS__REQUEST: "API__USER__GET__PERMISSIONS__REQUEST",
    API__USER__GET__PERMISSIONS__SUCCESS: "API__USER__GET__PERMISSIONS__SUCCESS",
    API__USER__GET__PERMISSIONS__ERROR: "API__USER__GET__PERMISSIONS__ERROR",

    API__USER__LIST_BY_MERCHANT__REQUEST: "API__USER__LIST_BY_MERCHANT__REQUEST",
    API__USER__LIST_BY_MERCHANT__SUCCESS: "API__USER__LIST_BY_MERCHANT__SUCCESS",
    API__USER__LIST_BY_MERCHANT__ERROR: "API__USER__LIST_BY_MERCHANT__ERROR",

    API__USER__INVITE__REQUEST: "API__USER__INVITE__REQUEST",
    API__USER__INVITE__SUCCESS: "API__USER__INVITE__SUCCESS",
    API__USER__INVITE__ERROR: "API__USER__INVITE__ERROR",

    API__USER__INVITE_CONFIRM__REQUEST: "API__USER__INVITE_CONFIRM__REQUEST",
    API__USER__INVITE_CONFIRM__SUCCESS: "API__USER__INVITE_CONFIRM__SUCCESS",
    API__USER__INVITE_CONFIRM__ERROR: "API__USER__INVITE_CONFIRM__ERROR",

    LOCAL__USER__CLEAR: "LOCAL__USER__CLEAR",
    LOCAL__USER__SET_STORE_ID: "LOCAL__USER__SET_STORE_ID",
}

const a = {
    requestCreate: createAction(c.API__USER__CREATE__REQUEST),
    successCreate: createAction(c.API__USER__CREATE__SUCCESS),
    errorCreate: createAction(c.API__USER__CREATE__ERROR),

    requestUpdate: createAction(c.API__USER__UPDATE__REQUEST),
    successUpdate: createAction(c.API__USER__UPDATE__SUCCESS),
    errorUpdate: createAction(c.API__USER__UPDATE__ERROR),

    requestPermissions: createAction(c.API__USER__GET__PERMISSIONS__REQUEST),
    successPermissions: createAction(c.API__USER__GET__PERMISSIONS__SUCCESS),
    errorPermissions: createAction(c.API__USER__GET__PERMISSIONS__ERROR),

    requestListByMerchant: createAction(c.API__USER__LIST_BY_MERCHANT__REQUEST),
    successListByMerchant: createAction(c.API__USER__LIST_BY_MERCHANT__SUCCESS),
    errorListByMerchant: createAction(c.API__USER__LIST_BY_MERCHANT__ERROR),

    requestInvite: createAction(c.API__USER__INVITE__REQUEST),
    successInvite: createAction(c.API__USER__INVITE__SUCCESS),
    errorInvite: createAction(c.API__USER__INVITE__ERROR),

    requestInviteConfirm: createAction(c.API__USER__INVITE_CONFIRM__REQUEST),
    successInviteConfirm: createAction(c.API__USER__INVITE_CONFIRM__SUCCESS),
    errorInviteConfirm: createAction(c.API__USER__INVITE_CONFIRM__ERROR),

    clearState: createAction(c.LOCAL__USER__CLEAR),
    setStoreId: createAction(c.LOCAL__USER__SET_STORE_ID),
}

export default { ...c, ...a }
