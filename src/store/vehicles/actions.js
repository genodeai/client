import { createAction } from "helpers/utility"

const c = {
    API__VEHICLES__CREATE__REQUEST: "API__VEHICLES__CREATE__REQUEST",
    API__VEHICLES__CREATE__SUCCESS: "API__VEHICLES__CREATE__SUCCESS",
    API__VEHICLES__CREATE__FAILED: "API__VEHICLES__CREATE__FAILED",

    API__VEHICLES__LIST__REQUEST: "API__VEHICLES__LIST__REQUEST",
    API__VEHICLES__LIST__SUCCESS: "API__VEHICLES__LIST__SUCCESS",
    API__VEHICLES__LIST__FAILED: "API__VEHICLES__LIST__FAILED",

    API__VEHICLES__GET__REQUEST: "API__VEHICLES__GET__REQUEST",
    API__VEHICLES__GET__SUCCESS: "API__VEHICLES__GET__SUCCESS",
    API__VEHICLES__GET__FAILED: "API__VEHICLES__GET__FAILED",

    API__VEHICLES__UPDATE__REQUEST: "API__VEHICLES__UPDATE__REQUEST",
    API__VEHICLES__UPDATE__SUCCESS: "API__VEHICLES__UPDATE__SUCCESS",
    API__VEHICLES__UPDATE__FAILED: "API__VEHICLES__UPDATE__FAILED",
}

const a = {
    requestCreate: createAction(c.API__VEHICLES__CREATE__REQUEST),
    successCreate: createAction(c.API__VEHICLES__CREATE__SUCCESS),
    failedCreate: createAction(c.API__VEHICLES__CREATE__FAILED),

    requestList: createAction(c.API__VEHICLES__LIST__REQUEST),
    successList: createAction(c.API__VEHICLES__LIST__SUCCESS),
    failedList: createAction(c.API__VEHICLES__LIST__FAILED),

    requestGet: createAction(c.API__VEHICLES__GET__REQUEST),
    successGet: createAction(c.API__VEHICLES__GET__SUCCESS),
    failedGet: createAction(c.API__VEHICLES__GET__FAILED),

    requestUpdate: createAction(c.API__VEHICLES__UPDATE__REQUEST),
    successUpdate: createAction(c.API__VEHICLES__UPDATE__SUCCESS),
    failedUpdate: createAction(c.API__VEHICLES__UPDATE__FAILED),
}

export default { ...c, ...a }
