import { takeEvery, put, call } from "redux-saga/effects"
// import { push } from "react-router-redux"

import aApp from "store/app/actions"

import a from "./actions"
import * as api from "./api"

function* handleCreate({ payload }) {
    try {
        let vehicle = yield call(api.create(payload.data))
        vehicle = yield call(api.upload(vehicle.uuid, payload.files))
        yield put(a.successCreate(vehicle))
        // yield put(push("/dashboard/vehicles/"))
        yield put(aApp.pushNotificationWithSuccess(
            "vehicles.create.success.message",
            "vehicles.create.success.description",
        ))
    } catch (e) {
        yield put(a.failedCreate())
    }
}

function* handleGet({ payload }) {
    try {
        const vehicle = yield call(api.getbyId(payload))
        yield put(a.successGet(vehicle))
    } catch (e) {
        yield put(a.failedGet())
    }
}

function* handleUpdate({ payload }) {
    try {
        const vehicle = yield call(api.updateById(payload.uuid, payload.data))
        yield put(a.successUpdate(vehicle))
        yield put(aApp.pushNotificationWithSuccess(
            "vehicles.update.success.message",
            "vehicles.update.success.description",
        ))
    } catch (e) {
        yield put(a.failedUpdate())
    }
}

function* handleList() {
    try {
        const vehicles = yield call(api.list())
        yield put(a.successList(vehicles))
    } catch (e) {
        yield put(a.failedList())
    }
}

export default function* () {
    yield takeEvery(a.API__VEHICLES__GET__REQUEST, handleGet)
    yield takeEvery(a.API__VEHICLES__UPDATE__REQUEST, handleUpdate)
    yield takeEvery(a.API__VEHICLES__CREATE__REQUEST, handleCreate)
    yield takeEvery(a.API__VEHICLES__LIST__REQUEST, handleList)
}
