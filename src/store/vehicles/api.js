import axios from "helpers/axios"

const BASE = "/api/v1/vehicles"

export const getbyId = id => function* () {
    return (yield axios.get(`${BASE}/${id}`)).data
}

export const list = () => function* () {
    return (yield axios.get(BASE)).data
}

export const create = params => function* () {
    return (yield axios.post(BASE, params)).data
}

export const updateById = (uuid, params) => function* () {
    return (yield axios.patch(`${BASE}/${uuid}`, params)).data
}

export const upload = (uuid, files) => function* () {
    const form = new FormData()
    files.forEach((file) => {
        if (file.main) {
            form.set("main", file.originFileObj)
        } else {
            form.append("list", file.originFileObj)
        }
    })

    return (yield axios.post(`${BASE}/${uuid}/pictures`, form, {
        headers: { "Content-Type": "multipart/form-data" },
    })).data
}
