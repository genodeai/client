import { assign } from "lodash/fp"

import a from "./actions"

const initialState = {
    data: [],
    fetching: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case a.API__VEHICLES__GET__REQUEST:
        case a.API__VEHICLES__LIST__REQUEST:
        case a.API__VEHICLES__UPDATE__REQUEST:
        case a.API__VEHICLES__CREATE__REQUEST: return assign(state, {
            fetching: true,
        })

        case a.API__VEHICLES__GET__SUCCESS:
        case a.API__VEHICLES__UPDATE__SUCCESS:
            let done = false
            const data = state.data.map((item) => {
                if (item.uuid === action.payload.uuid) {
                    done = true
                    return action.payload
                }
                return item
            })
            if (!done) {
                data.push(action.payload)
            }
            return assign(initialState, { data })
        case a.API__VEHICLES__LIST__SUCCESS: return assign(initialState, {
            data: action.payload,
        })
        case a.API__VEHICLES__CREATE__SUCCESS: return assign(initialState, {
            data: state.data.concat(action.payload),
        })

        case a.API__VEHICLES__GET__FAILED:
        case a.API__VEHICLES__LIST__FAILED:
        case a.API__VEHICLES__UPDATE__FAILED:
        case a.API__VEHICLES__CREATE__FAILED: return assign(state, {
            fetching: false,
        })

        // case a.API__LOCATION__UPDATE__SUCCESS: return assign(initialState, {
        //     data: state.data.map((item) => {
        //         if (item.storeId === action.payload.storeId) {
        //             return action.payload
        //         }
        //         return item
        //     }),
        // })

        // case a.API__LOCATION__DELETE__SUCCESS: return assign(initialState, {
        //     data: state.data.filter(item => item.storeId !== action.payload),
        // })

        default:
            return state
    }
}
