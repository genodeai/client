export function getView(width) {
    let newView = "MobileView"
    if (width > 1220) {
        newView = "DesktopView"
    } else if (width > 767) {
        newView = "TabView"
    }
    return newView
}
const c = {
    LOCAL__APP__COLLAPSE_CHANGE: "LOCAL__APP__COLLAPSE_CHANGE",
    LOCAL__APP__COLLAPSE_OPEN_DRAWER: "LOCAL__APP__COLLAPSE_OPEN_DRAWER",
    LOCAL__APP__CHANGE_OPEN_KEYS: "LOCAL__APP__CHANGE_OPEN_KEYS",
    LOCAL__APP__TOGGLE_ALL: "LOCAL__APP__TOGGLE_ALL",
    LOCAL__APP__CHANGE_CURRENT: "LOCAL__APP__CHANGE_CURRENT",
    LOCAL__APP__SET_LOCALE: "LOCAL__APP__SET_LOCALE",
    LOCAL__APP__PUSH_NOTIFICATION__SUCCESS: "LOCAL__APP__PUSH_NOTIFICATION__SUCCESS",
    LOCAL__APP__PUSH_NOTIFICATION__ERROR: "LOCAL__APP__PUSH_NOTIFICATION__ERROR",
}
const a = {
    toggleCollapsed: () => ({
        type: c.LOCAL__APP__COLLAPSE_CHANGE,
    }),
    toggleAll: (width, height) => {
        const view = getView(width)
        const collapsed = view !== "DesktopView"
        return {
            type: c.LOCAL__APP__TOGGLE_ALL,
            collapsed,
            view,
            height,
        }
    },
    toggleOpenDrawer: () => ({
        type: c.LOCAL__APP__COLLAPSE_OPEN_DRAWER,
    }),
    changeOpenKeys: openKeys => ({
        type: c.LOCAL__APP__CHANGE_OPEN_KEYS,
        openKeys,
    }),
    changeCurrent: (current, redirect = false) => ({
        type: c.LOCAL__APP__CHANGE_CURRENT,
        payload: { current, redirect },
    }),
    pushNotificationWithSuccess: (message, description = null) => ({
        type: c.LOCAL__APP__PUSH_NOTIFICATION__SUCCESS,
        payload: { message, description },
    }),
    pushNotificationWithError: (message, description = null) => ({
        type: c.LOCAL__APP__PUSH_NOTIFICATION__ERROR,
        payload: { message, description },
    }),
    setLocale: payload => ({
        type: c.LOCAL__APP__SET_LOCALE,
        payload,
    }),
}
export default { ...c, ...a }
