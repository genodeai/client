import React from "react"
import { takeEvery, put, select } from "redux-saga/effects"
import { push } from "react-router-redux"

import { IntlMessage, LocaleProvider } from "components/commons/utility"
import { notification } from "components/commons/feedback"

import a from "./actions"
import { getLocale } from "./selector"

function* createNotification({ type, message, description }, options = {}) {
    const locale = yield select(getLocale)
    const getIntlMessage = path => (
        <LocaleProvider locale={locale}>
            <IntlMessage id={`notification.${path}`} {...options} />
        </LocaleProvider>
    )
    notification[type]({
        message: getIntlMessage(message),
        description: getIntlMessage(description),
    })
}

function* handleChangeCurrent({ payload }) {
    if (payload.redirect === true) {
        yield put(push(payload.current))
    }
}
function* handlePushNotificationWithSuccess({ payload }) {
    yield createNotification({ type: "success", ...payload })
}
function handlePushNotificationWithError({ payload }) {
    notification.error({
        message: payload.message,
        description: payload.description,
    })
}

export default function* () {
    yield takeEvery(a.LOCAL__APP__CHANGE_CURRENT, handleChangeCurrent)
    yield takeEvery(a.LOCAL__APP__PUSH_NOTIFICATION__SUCCESS, handlePushNotificationWithSuccess)
    yield takeEvery(a.LOCAL__APP__PUSH_NOTIFICATION__ERROR, handlePushNotificationWithError)
}
