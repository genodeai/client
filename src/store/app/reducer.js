import { assign } from "lodash/fp"

import config, { getCurrentLanguage } from "i18n/config"

import a, { getView } from "./actions"

const initState = {
    collapsed: !(window.innerWidth > 1220),
    view: getView(window.innerWidth),
    height: window.innerHeight,
    openDrawer: false,
    openKeys: [],
    current: "",
    locale: getCurrentLanguage(config.defaultLanguage || "english").locale,
}
export default function (state = initState, action) {
    switch (action.type) {
        case a.LOCAL__APP__COLLAPSE_CHANGE: return assign(state, {
            collapsed: !state.collapsed,
        })
        case a.LOCAL__APP__COLLAPSE_OPEN_DRAWER: return assign(state, {
            openDrawer: !state.openDrawer,
        })
        case a.LOCAL__APP__TOGGLE_ALL:
            if (state.view !== action.view || action.height !== state.height) {
                const height = action.height ? action.height : state.height
                return assign(state, {
                    collapsed: action.collapsed,
                    view: action.view,
                    height,
                })
            }
            return state
        case a.LOCAL__APP__CHANGE_OPEN_KEYS: return assign(state, {
            openKeys: action.openKeys,
        })
        case a.LOCAL__APP__CHANGE_CURRENT: return assign(state, {
            current: action.payload.current,
        })
        case a.LOCAL__APP__SET_LOCALE: return assign(state, {
            locale: action.payload,
        })

        default: return state
    }
}
