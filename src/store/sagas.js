import { all, fork } from "redux-saga/effects"

import watcherApp from "./app/saga"
import watcherVehicles from "./vehicles/saga"

export default function* rootSaga() {
    yield all([
        fork(watcherApp),
        fork(watcherVehicles),
    ])
}
