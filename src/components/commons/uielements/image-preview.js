import React from "react"
import PropTypes from "prop-types"

import { Container, Frame } from "./style/image-preview.style"

export default class ImagePreview extends React.Component {
    static propTypes = {
        image: PropTypes.shape({
            originFileObj: PropTypes.instanceOf(File).isRequired,
        }),
        link: PropTypes.string,
    }
    static defaultProps = {
        link: "",
        image: null,
    }
    state = {
        image: null,
    }
    componentDidMount() {
        if (this.props.image) {
            const reader = new FileReader()
            reader.onloadend = e => this.setState({ image: e.target.result })
            reader.readAsDataURL(this.props.image.originFileObj)
        }
    }
    render() {
        if (!this.props.link && !this.state.image) {
            return null
        }
        return (
            <Container>
                <Frame>
                    <img src={this.props.link || this.state.image} />
                </Frame>
            </Container>
        )
    }
}
