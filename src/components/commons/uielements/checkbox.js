import React from "react"
import { Checkbox as AntCheckbox } from "antd"
import PropTypes from "prop-types"
import classNames from "classnames"

import styles from "./styles/checkbox.module.scss"

const Checkbox = ({ className, ...props }) => (
    <AntCheckbox
        className={classNames({
            [styles.container]: true,
            [className]: !!className,
        })}
        {...props}
    />
)
Checkbox.defaultProps = {
    className: "",
}
Checkbox.propTypes = {
    className: PropTypes.string,
}
const CheckboxGroup = AntCheckbox.Group

export default Checkbox
export { CheckboxGroup }
