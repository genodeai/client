import { AutoComplete } from "antd"

import WithDirection from "styles/config/withDirection"
import { AntAutoComplete } from "./styles/autoComplete.style"

const WDAutoCompletes = AntAutoComplete(AutoComplete)
const AutoCompletes = WithDirection(WDAutoCompletes)
const AutoCompleteOption = AutoComplete.Option

export default AutoCompletes
export { AutoCompleteOption }
