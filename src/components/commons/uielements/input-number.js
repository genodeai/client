import { InputNumber as AntInputNumber } from "antd"

import { InputNumberWrapper } from "./style/input-number.style"

export default InputNumberWrapper(AntInputNumber)
