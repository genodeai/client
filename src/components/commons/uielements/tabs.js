import { Tabs as AntTabs } from "antd"

import { TabsWrapper } from "./style/tabs.style"

const Tabs = TabsWrapper(AntTabs)

export default Tabs
