import { Transfer } from "antd"
import WithDirection from "styles/config/withDirection"
import AntTransfer from "./styles/transfer.style"

const WDTransfers = AntTransfer(Transfer)
const Transfers = WithDirection(WDTransfers)

export default Transfers
