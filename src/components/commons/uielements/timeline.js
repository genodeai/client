import { Timeline } from "antd"
import WithDirection from "styles/config/withDirection"
import AntTimeline from "./styles/timeline.style"

const Timelines = AntTimeline(Timeline)
const WDTimelineItem = AntTimeline(Timeline.Item)
const TimelineItem = WithDirection(WDTimelineItem)

export default WithDirection(Timelines)
export { TimelineItem }
