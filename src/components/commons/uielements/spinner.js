
import React from "react"
import styled from "styled-components"
import Icon from "./icon"

const Spinner = styled(Icon)`
  font-size: 34px;
  color: #4482FF;
`

const SpinnerGroup = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

export default () => (
    <SpinnerGroup>
        <Spinner type="loading" spin />
    </SpinnerGroup>
)

