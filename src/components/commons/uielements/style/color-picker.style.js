import styled from "styled-components"

export const ColorPickerWrapper = component => styled(component)`
    .rc-color-picker-trigger {
        vertical-align: middle;
    }
`
