import styled from "styled-components"

import { palette } from "styles/constants"

export const InputNumberWrapper = component => styled(component)`
    &.ant-input-number {
        color: ${palette.text[1]};
        padding: 0;
        font-size: 13px;
        height: 35px !important;
        border: 1px solid ${palette.border[0]};
        width: 100%;
        max-width: 80px;

        .ant-input-number-input {
            height: 33px;
            color: ${palette.text[1]};
            padding: 0 10px;
            text-align: left;
        }

        .ant-input-number-focused {
            border-color: ${palette.primary[0]};
        }

        .ant-input-number-handler-wrap {
            right: 0;
            border-radius: 0 4px 4px 0;
            border-left: 1px solid ${palette.border[1]};
        }

        .ant-input-number-handler-up-inner,
        .ant-input-number-handler-down-inner {
            font-size: 23px;
            color: ${palette.text[2]};

            &:before {
                display: block;
                font-family: 'ionicons' !important;
            }
        }

        .ant-input-number-handler-up-inner {
            &:before {
                content: '\f365';
            }
        }

        .ant-input-number-handler-down-inner {
            &:before {
            content: '\f35f';
            }
        }
    }
`
