import styled from "styled-components"
import { palette } from "styles/constants"

export const Container = styled.div`
    img {
        width: 100%;
        height: auto;
    }
`

export const Frame = styled.div`
    padding: 8px;
    border: 1px solid ${palette.grayscale[0]};
    border-radius: 5px;
`
