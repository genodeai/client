import { Button as AntButton } from "antd"

import { ButtonGroupWrapper, ButtonWrapper } from "./style/button.style"

const Button = ButtonWrapper(AntButton)
Button.Group = ButtonGroupWrapper(AntButton.Group)

export default Button
