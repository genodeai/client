import ColorPicker from "rc-color-picker"
import "rc-color-picker/assets/index.css"

import { ColorPickerWrapper } from "./style/color-picker.style"

export default ColorPickerWrapper(ColorPicker)
