import { Tree } from "antd"
import WithDirection from "styles/config/withDirection"
import AntTree from "./styles/tree.style"

const WDTrees = AntTree(Tree)
const Trees = WithDirection(WDTrees)

const { TreeNode } = Tree

export default Trees
export { TreeNode }
