import React from "react"
import { compose, withProps } from "recompose"
import PropTypes from "prop-types"
import {
    GoogleMap,
    Marker,
    Polygon,
    Circle,
    withGoogleMap,
} from "react-google-maps"

import { DrawingManager } from "react-google-maps/lib/components/drawing/DrawingManager"
import styles from "./styles/map.module.scss"

class Map extends React.Component {
    static POLYGON = "polygon"
    static CIRCLE = "circle"

    static propTypes = {
        defaultCenter: PropTypes.object,
        onAreaComplete: PropTypes.func.isRequired,
        shapeType: PropTypes.string,
        defaultZoom: PropTypes.number,
        paths: PropTypes.array,
        markerPosition: PropTypes.object,
        circleCenter: PropTypes.object,
        circleRadius: PropTypes.number,
    }

    static defaultProps = {
        circleCenter: null,
        circleRadius: 0,
        shapeType: Map.CIRCLE,
        defaultCenter: { lat: -34.397, lng: 150.644 },
        defaultZoom: 8,
        paths: [],
        markerPosition: undefined,
    }

    componentWillReceiveProps(nextProps) {
        const { OverlayType } = google.maps.drawing
        const bounds = new window.google.maps.LatLngBounds()
        if (nextProps.shapeType === OverlayType.POLYGON && nextProps.paths.length) {
            nextProps.paths.forEach(({ lat, lng }) => bounds.extend({ lat, lng }))
            this.map.fitBounds(bounds)
        }
        if (nextProps.shapeType === OverlayType.CIRCLE && nextProps.circleCenter) {
            const circle = new window.google.maps.Circle({
                center: nextProps.circleCenter,
                radius: nextProps.circleRadius,
            })
            this.map.fitBounds(circle.getBounds())
        }
    }

    render() {
        const {
            paths,
            shapeType,
            defaultCenter,
            defaultZoom,
            markerPosition,
            circleCenter,
            circleRadius,
        } = this.props
        return (
            <GoogleMap
                ref={(ref) => { this.map = ref }}
                defaultZoom={defaultZoom}
                defaultCenter={defaultCenter}
            >
                {paths.length === 0 && circleRadius === 0 && (
                    <DrawingManager
                        defaultDrawingMode={google.maps.drawing.OverlayType.POLYGON}
                        onPolygonComplete={this.handlePolygonComplete}
                        onCircleComplete={this.handleCircleComplete}
                        options={{
                            drawingControl: true,
                            drawingControlOptions: {
                                position: google.maps.ControlPosition.TOP_CENTER,
                                drawingModes: [
                                    google.maps.drawing.OverlayType.POLYGON,
                                    google.maps.drawing.OverlayType.CIRCLE,
                                ],
                            },
                        }}
                    />
                )}

                {paths.length !== 0 && shapeType === Map.POLYGON && (
                    <Polygon paths={paths} />
                )}

                {circleRadius !== 0 && shapeType === Map.CIRCLE && (
                    <Circle center={circleCenter} radius={circleRadius} />
                )}

                {markerPosition && (<Marker position={markerPosition} />)}
            </GoogleMap>
        )
    }
    handlePolygonComplete = (polygon) => {
        const paths = []
        const path = polygon.getPath()
        path.forEach(point => paths.push(point.toJSON()))
        path.clear()
        this.props.onAreaComplete({ paths, shapeType: Map.POLYGON })
    }
    handleCircleComplete = (circle) => {
        const circleRadius = circle.getRadius()
        const circleCenter = circle.getCenter().toJSON()
        circle.setMap(null)
        this.props.onAreaComplete({ circleCenter, circleRadius, shapeType: Map.CIRCLE })
    }
}

const enhance = compose(
    withProps({
        loadingElement: <div className={styles.loading} />,
        containerElement: <div className={styles.container} />,
        mapElement: <div className={styles["map-element"]} />,
    }),
    withGoogleMap
)


export default enhance(Map)
