import React from "react"
import PropTypes from "prop-types"

import { IntlMessage } from "components/commons/utility"

import * as s from "./style/lobby.style"

class Lobby extends React.Component {
    static FormItem = s.FormItem
    static FormFeedback = s.FormFeedback
    static Divider = s.Divider
    static Links = s.Links

    static propTypes = {
        onSubmit: PropTypes.func.isRequired,
        className: PropTypes.string.isRequired,
        children: PropTypes.node.isRequired,
        formValid: PropTypes.bool,
        title: PropTypes.string,
    }
    static defaultProps = {
        formValid: true,
        title: "lobby.title",
    }
    render() {
        return (
            <s.Container className={this.props.className}>
                <s.ContentWrapper>
                    <s.Content>
                        <s.LogoWrapper>
                            <IntlMessage id={this.props.title} />
                        </s.LogoWrapper>
                        <s.Form onKeyPress={this.handleKeyPress}>
                            { this.props.children }
                        </s.Form>
                    </s.Content>
                </s.ContentWrapper>
            </s.Container>
        )
    }
    handleKeyPress = (e) => {
        if (e.charCode === 13 && this.props.formValid) {
            this.props.onSubmit()
        }
    }
}

export default Lobby
