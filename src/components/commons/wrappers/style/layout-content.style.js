import styled from "styled-components"

import { palette } from "styles/constants"

export const Container = styled.div`
    width: 900px;
    margin: 40px auto 0;
    .ant-form-item {
        display: flex;
    }
    .ant-form-item-label {
        text-align: left;
        width: 10% !important;
        margin-right: 20px !important;
    }
    .ant-form-item-control-wrapper {
        width: 90% !important;
    }
    &:not(.ant-form-item-label) .ant-form-item-control-wrapper {
        width: 100% !important;
    }
`

export const Toolbar = styled.div`
    width: 100%;
    height: 50px;
    display: flex;
    align-items: center;
    margin-bottom: 10px;
`

export const Actions = styled.div`
    display: flex;
    justify-content: flex-start;
    & > * {
        margin-right: 10px;
    }
    &:not(:first-child) {
        margin-left: 20px;
    }
    button {
        font-size: 16px;
    }

`

export const Content = styled.div`
    padding: 10px;
    background-color: #ffffff;
    border: 1px solid ${palette.border[0]};
    height: 100%;
    margin-bottom: 20px;
    h2 {
        margin-bottom: 20px;
    }
`
