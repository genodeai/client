import styled from "styled-components"
import { palette } from "styles/constants"

import { Form as CommonForm } from "components/commons/uielements"

export const Container = styled.div`
    width: 100%;
    min-height: 100vh;
    height: 100vh;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    position: relative;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  
    &:before {
        content: '';
        width: 100%;
        height: 100%;
        display: flex;
        background-color: rgba(0, 0, 0, 0.6);
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        right: inherit;
    }
}
`

export const ContentWrapper = styled.div`
    width: 500px;
    height: 100%;
    overflow-y: auto;
    z-index: 10;
    position: relative;
`

export const Content = styled.div`
    min-height: 100%;
    display: flex;
    flex-direction: column;
    padding: 70px 50px;
    position: relative;
    background-color: #ffffff;

    @media only screen and (max-width: 767px) {
        width: 100%;
        padding: 70px 20px;
    }
`

export const LogoWrapper = styled.div`
    width: 100%;
    display: flex;
    margin-bottom: 50px;
    justify-content: center;
    flex-shrink: 0;

    span {
        font-size: 24px;
        font-weight: 300;
        line-height: 1;
        text-transform: uppercase;
        color: ${palette.secondary[2]};
    }
`

export const Form = styled(CommonForm)`
    width: 100%;
    flex-shrink: 0;
    display: flex;
    flex-direction: column;

    .head {
        width: 100%;
        display: flex;
        flex-direction: column;
        margin-bottom: 15px;
        justify-content: center;

        h3 {
            font-size: 14px;
            font-weight: 500;
            line-height: 1.2;
            margin: 0 0 7px;
            color: $paletteText0;
        }

        p {
            font-size: 13px;
            font-weight: 400;
            line-height: 1.2;
            margin: 0;
            color: $paletteText2;
        }
    }
    .token-validation {
        & > .ant-spin {
            padding-right: 15px;
        }
    }

    button {
        font-weight: 500;
    }
`
export const FormItem = styled(CommonForm.Item)`
    margin-bottom: 15px;

    &:last-child {
        margin-bottom: 0;
    }
`

export const FormFeedback = styled.div`
    margin-bottom: 25px;
`

export const Divider = styled.div`
    border-top: 1px dashed ${palette.grayscale[2]};
`

export const Links = styled.div`
    margin-top: 35px;
    flex-direction: column;

    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
`
