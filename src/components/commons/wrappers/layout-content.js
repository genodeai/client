import React from "react"
import PropTypes from "prop-types"

import { Button } from "components/commons/uielements"

import * as s from "./style/layout-content.style"

const LayoutContentWrapper = props => (
    <s.Container>
        <s.Toolbar>
            {!!props.title === true && (<h2>{props.title}</h2>)}
            <s.Actions>
                { props.actions.map((item, index) => {
                    const additional = {
                        icon: item.icon || null,
                        loading: item.loading || false,
                    }
                    return (
                        <Button
                            key={String(index)}
                            onClick={item.handler}
                            disabled={!!item.disabled}
                            {...additional}
                        >
                            { item.title }
                        </Button>
                    )
                })}
            </s.Actions>
        </s.Toolbar>
        { Array.isArray(props.children) === true && props.multi === true
            ? props.children.map(item => item !== false && (
                <s.Content>
                    { item }
                </s.Content>
            )) : (
                <s.Content>
                    { props.children }
                </s.Content>
            )}
    </s.Container>
)

LayoutContentWrapper.defaultProps = {
    title: (<span />),
    actions: [],
    multi: false,
}
LayoutContentWrapper.propTypes = {
    title: PropTypes.node,
    multi: PropTypes.bool,
    actions: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.node,
        icon: PropTypes.string,
        handler: PropTypes.func,
        disabled: PropTypes.bool,
        loading: PropTypes.bool,
    })),
    children: PropTypes.oneOfType([
        PropTypes.node.isRequired,
        PropTypes.arrayOf(PropTypes.node),
    ]).isRequired,
}

export default LayoutContentWrapper
