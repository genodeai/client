import React from "react"
import { get, set, has, cloneDeep } from "lodash"
import { classToPlain } from "class-transformer"

import { transformAndValidateEntitySync } from "platform-server/dist/common/validator.utility"

export default class EntityForm extends React.Component {
    static STATUS = {
        ERROR: "error",
        SUCCESS: "success",
        NULL: null,
    }
    state = {
        data: {},
        errors: [],
    }

    init(state = this.state, options = {}) {
        if (options.Entity) {
            this.Entity = options.Entity
        }
        if (!this.Entity) {
            throw new Error("Entity is not defined")
        }
        if (options.entityValidationOptions) {
            this.entityValidationOptions = {
                validator: {
                    groups: options.entityValidationOptions.groups,
                    whitelist: true,
                    forbidNonWhitelisted: true,
                    forbidUnknownValues: true,
                    validationError: { target: false, value: false },
                },
                transformer: {
                    groups: options.entityValidationOptions.groups,
                },
            }
        }
        if (!this.entityValidationOptions) {
            throw new Error("Entity validation options is not defined")
        }

        const nextState = {
            ...this.state,
            ...state,
            data: this.getTransformed(state.data || {}),
            errors: state.errors || this.state.errors,
        }
        if (options.updateState) {
            this.setState(nextState)
        } else {
            this.state = nextState
        }
    }
    getTransformed = (data = this.state.data, Entity) => classToPlain(
        new (Entity || this.Entity)().init(data),
        this.entityValidationOptions.validator
    )


    isFormValid = (validate) => {
        if (this.state.errors.length > 0) {
            return false
        }
        try {
            transformAndValidateEntitySync(this.Entity, this.state.data, this.entityValidationOptions)
        } catch (e) {
            console.warn(e)
            const found = e.some(item => !!has(this.state.data, item.path))
            if (found) return false
        }

        if (typeof validate === "function") {
            return validate()
        }
        return true
    }
    getPropsBlur = field => () => {
        try {
            transformAndValidateEntitySync(this.Entity, this.state.data, this.entityValidationOptions)
        } catch (e) {
            const error = this.findError(field, e)
            if (error && !this.findError(field)) {
                this.setState({ errors: this.state.errors.concat(error) })
            }
        }
    }
    getProps = field => ({
        onChange: value => this.handleState(field, value),
        value: get(this.state.data, Array.isArray(field) ? field[0] : field),
        onBlur: this.getPropsBlur(field),
    })
    getInputProps = field => ({
        ...this.getProps(field),
        onChange: (e) => {
            e.persist()
            const { value } = e.target
            this.handleState(field, value)
        },
    })
    getSwitchProps = field => ({
        onChange: value => this.handleState(field, value),
        checked: get(this.state.data, field),
    })
    getGeosuggestProps = (prefix = "", field, mapping) => {
        const selector = f => (prefix ? `${prefix}.${f}` : f)
        return ({
            initialValue: get(this.state.data, selector(field)),
            inputClassName: "ant-input",
            onBlur: this.getPropsBlur(selector),
            onSuggestSelect: (suggest) => {
                if (suggest) {
                    const { label, location } = suggest
                    let data = cloneDeep(this.state.data)
                    if (mapping.lat && location.lat) {
                        data = set(data, selector(mapping.lat), location.lat)
                    }
                    if (mapping.lng && location.lng) {
                        data = set(data, selector(mapping.lng), location.lng)
                    }
                    if (mapping.label && label) {
                        data = set(data, selector(mapping.label), label)
                    }
                    this.setState({ data })
                }
            },
        })
    }
    getColorPickerProps = field => ({
        color: get(this.state.data, field),
        onChange: ({ color }) => this.handleState(field, color),
    })

    getStatus = (field) => {
        const error = this.findError(field)
        if (error) {
            return {
                validateStatus: EntityForm.STATUS.ERROR,
                help: error.message,
            }
        }
        return {}
    }

    findError = (field, errors = this.state.errors) => errors.find(item => item.path === field)
    handleState = (field, value) => {
        let data = cloneDeep(this.state.data)
        if (!Array.isArray(field)) {
            data = set(data, field, value)
        } else {
            field.forEach((item) => { data = set(data, item, value) })
        }
        this.setState({
            ...this.state,
            errors: this.state.errors.filter(item => item.path !== field),
            data,
        })
    }
}
