import React from "react"
import { get, set } from "lodash"

import { IntlMessages } from "components/commons/utility"
import { isPasswordValid, isEmailValid } from "helpers/validators"

const getIntlMessage = field => (
    <IntlMessages id={`form-helper.${field}`} />
)

export default class FormHelper extends React.Component {
    static STATUS = {
        ERROR: "error",
        SUCCESS: "success",
        NULL: null,
    }
    state = {
        errors: [],
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.errors !== undefined && "message" in nextProps.errors) {
            this.setState(this.getStateErrors("general", { add: true }))
        }
    }

    setStateData = payload => this.setState(s => ({ data: { ...s.data, ...payload } }))
    getStateErrors = (field, params = {}) => {
        const filter = () => this.state.errors.filter((e) => {
            if (Array.isArray(field) === true) {
                return !field.some(fe => e === fe)
            }
            return e !== field
        })
        const concat = () => this.state.errors.concat(field)
        const getResult = func => ({ errors: func() })
        if (params.add === true) {
            return getResult(concat)
        }
        if (params.exclude === true) {
            return getResult(filter)
        }
        if (typeof params.validator === "function"
        && params.validator(get(this.state, field)) === false
        && this.state.errors.includes(field) === false) {
            return getResult(concat)
        }
        return this.state.errors
    }

    getInputProps = (field, validator) => ({
        ...this.getProps(field, validator),
        onChange: (e) => {
            e.persist()
            const { value } = e.target
            this.handleState(field, value)
        },
    })

    getProps = (field, validator = () => !!get(this.state, field)) => ({
        onChange: (value) => {
            this.handleState(field, value)
        },
        value: get(this.state, field),
        onBlur: () => this.setState(this.getStateErrors(field, { validator })),
    })

    handleState = (field, value) => {
        const nextState = {
            ...this.state,
            ...this.getStateErrors(["general", field], { exclude: true }),
        }
        this.setState(set(nextState, field, value))
    }

    getEmailStatus = (field = "email") => {
        if (this.state.errors.includes(field) === true) {
            if (isEmailValid(this.state[field]) === false) {
                return {
                    validateStatus: FormHelper.STATUS.ERROR,
                    help: getIntlMessage("email"),
                }
            }
            return { validateStatus: FormHelper.STATUS.SUCCESS }
        }
        return {}
    }
    getPasswordStatus() {
        if (this.state.errors.includes("password") === true) {
            if (isPasswordValid(this.state.password) === false) {
                return {
                    validateStatus: FormHelper.STATUS.ERROR,
                    help: getIntlMessage("password"),
                }
            }
            return { validateStatus: FormHelper.STATUS.SUCCESS }
        }
        return {}
    }
    getConfirmStatus() {
        if (this.state.errors.includes("confirmation") === true) {
            if (this.state.password !== this.state.confirmation) {
                return {
                    validateStatus: FormHelper.STATUS.ERROR,
                    help: getIntlMessage("confirmation"),
                }
            }
            return { validateStatus: FormHelper.STATUS.SUCCESS }
        }
        return {}
    }
    getSimpleStatus(field, message) {
        if (this.state.errors.includes(field) === true) {
            return {
                validateStatus: FormHelper.STATUS.ERROR,
                help: message || getIntlMessage("empty"),
            }
        }
        return {}
    }
}
