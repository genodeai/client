import React from "react"
import PropTypes from "prop-types"
import { LocaleProvider as AntLocaleProvider } from "antd"
import { IntlProvider } from "react-intl"

import AppLocale from "i18n"

const LocaleProvider = (props) => {
    const currentAppLocale = AppLocale[props.locale]
    return (
        <AntLocaleProvider locale={currentAppLocale.antd}>
            <IntlProvider
                locale={currentAppLocale.locale}
                messages={currentAppLocale.messages}
            >
                { props.children }
            </IntlProvider>
        </AntLocaleProvider>
    )
}
LocaleProvider.propTypes = {
    locale: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
}

export default LocaleProvider
