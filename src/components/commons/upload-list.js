import React from "react"
import styled from "styled-components"

import { Upload, Modal, Icon } from "./uielements"

const Img = styled.img`
    width: 100%;
`

export default class UploadList extends React.Component {
    state = {
        files: [],
        preview: "",
        previewVisible: false,
    }
    render() {
        const { files, preview, previewVisible } = this.state
        return (
            <div>
                <Upload
                    listType="picture-card"
                    fileList={files}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                >
                    {files.length < 3 && (
                        <div>
                            <Icon type="plus" />
                            <div className="ant-upload-text">Upload</div>
                        </div>
                    )}
                </Upload>
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <Img alt="example" src={preview} />
                </Modal>
            </div>
        )
    }
}
