import { notification } from "antd"

notification.config({
    top: 60,
    placement: "topRight",
})

export default notification
