import styled from "styled-components"

import { Card } from "components/commons/uielements"
import { palette } from "styles/constants"

const cardPaddings = "18px"

export const Container = styled.div`
    .ant-col-7 {
        width: 32.166667%;
        &:nth-child(2) {
            margin: 0 15px;
        }
    }
    .ant-card {
        margin-bottom: 10px;
        display: inline-table;
    }
    .ant-card-meta-title {
        padding: ${cardPaddings};
        padding-bottom: 0;
    }
    .ant-card-body {
        padding: 0;
    }
`

export const PlusVehicle = styled(Card)`
    width: 100%;
    height: 416px;
    display: grid !important;
    .ant-card-cover {   
        margin: auto auto;
    }
    .anticon.anticon-plus {
        font-size: 270px;
        color: ${palette.text[2]};
        padding: 40px 0;
        &:hover {
            color: ${palette.text[3]};
        }
    }
    .ant-card-body {
        display: none;  
    }
`

export const CardCover = styled.img`
    object-fit: cover;
    object-position: center;
    height: 220px;
    width: 100%;
`

export const CardDescription = styled.div`
    p > span:nth-child(1) {
        color: ${palette.text[0]};
        padding-right: 5px;
    }
    p > span:nth-child(2) {
        color: ${palette.text[1]};
    }
`
CardDescription.Info = styled.div`
    padding: ${cardPaddings};
    padding-top: 0;
`
CardDescription.Price = styled.div`
    width: 100%;
    height: 40px;
    line-height: 40px;
    font-size: 18px;
    font-weight: bold;
    background-color: red;
    color: white;
    /* text-align: center; */
    padding: 0 ${cardPaddings}
    vertical-align: middle;
`
