import styled from "styled-components"

import { palette } from "styles/constants"

export const Container = styled.div`
    .ant-tabs {
        margin-top: 20px;
    }
`

export const General = styled.div`
    display: flex;
`
General.Pictures = styled.div`
    width: 60%;
`
General.Pictures.Main = styled.div`
    img {
        object-fit: cover;
        object-position: center;
        height: 420px;
        width: 100%;
    }
`
General.Pictures.List = styled.div`
    margin-top: 18px;
    img {
        width: 72px;
        height: 72px;
        object-fit: cover;
        object-position: center;
        &:not(:last-child) {
            margin-right: 8px;
        }
    }
`

General.Description = styled.div`
    width: 40%;
    padding-left: 18px;
    h2 { 
        margin-bottom: 4px;
    }
    h3:not(:first-of-type) {
        margin-top: 18px;
        color: ${palette.color[4]}
        font-size: xx-large;
    }
`
export const DetailsListItem = {}
DetailsListItem.Title = styled.div`
    width: 40%;
`
DetailsListItem.Value = styled.div`
    width: 60%;
`
