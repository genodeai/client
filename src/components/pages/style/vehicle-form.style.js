import styled from "styled-components"

import { Row } from "components/commons/grid"

export const Container = styled.div`
    .ant-row-flex:not(.ant-row-flex-space-between) .ant-col-7:first-child {
        margin-right: 6.3%;
    }
    .ant-col-11 .ant-form-item-label {
        width: 34% !important;
    }
    .ant-col-7 .ant-form-item-label {
        width: 70% !important;
    }
    .ant-upload-select-picture-card i {
        font-size: 32px;
        color: #999;
    }

    .ant-upload-select-picture-card .ant-upload-text {
        margin-top: 8px;
        color: #666;
    }
    .ant-input-number {
        max-width: none;
    }
`

export const Pictures = styled.div`
    & > span:not(:first-child) {
        margin-right: 15px;
    }
    button {
        width: 65%;
        margin-top: 4px;
    }
    .ant-divider {
        margin-top: 8px;
    }
`

export const ImageRow = styled(Row)`
    margin-top: 12px;
    margin-bottom: 8px;
`
