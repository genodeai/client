import React from "react"
import { connect } from "react-redux"
import { isEqual } from "lodash"
import { VEHICLE_PICTURES_LIST_LIMIT } from "platform-server/dist/common/constants"

import {
    Form, Input, Switch,
    InputNumber, Select,
    ColorPicker, Upload, Icon,
    Button, Divider,
    ImagePreview,
} from "components/commons/uielements"
import { Row, Col } from "components/commons/grid"
import { EntityForm, IntlMessage } from "components/commons/utility"
import { LayoutContent } from "components/commons/wrappers"
import { VehicleModel } from "models/vehicle"
import aVehicles from "store/vehicles/actions"
import {
    BodyStyleEnum,
    PowerTypeEnum,
    FuelEnum,
    TransmissionEnum,
    ConditionEnum,
    BrandEnum,
} from "platform-server/dist/vehicle/interface"

import { Container, ImageRow, Pictures } from "./style/vehicle-form.style"

const getIntlMessage = (path = "") => (
    <IntlMessage id={`page.vehicle-form.${path}`} />
)

class VehicleForm extends EntityForm {
    constructor(props) {
        super(props)

        const { selected } = props.vehicles
        this.init({
            files: [],
            data: selected,
        }, this.getInitOptions(selected, false))
    }
    componentDidMount() {
        const { id } = this.props.match.params
        if (id) {
            this.props.get(id)
        }
    }
    componentWillReceiveProps(nextProps) {
        const { fetching, selected } = nextProps.vehicles
        if (!fetching && selected && this.isFormChanged(selected)) {
            this.init({
                data: selected,
            }, this.getInitOptions(nextProps.vehicles.selected))
        }
    }
    render() {
        const formValid = this.isFormValid()
        const { selected } = this.props.vehicles
        console.log(this.state.data, selected)
        return (
            <Container>
                <LayoutContent>
                    <Row type="flex" justify="space-between">
                        <Col span={11}>
                            <Form.Item
                                label={getIntlMessage("data.brand")}
                                {...this.getStatus("brand")}
                            >
                                <Select {...this.getProps("brand")} showSearch>
                                    {Object.values(BrandEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={11}>
                            <Form.Item
                                label={getIntlMessage("data.model")}
                                {...this.getStatus("model")}
                            >
                                <Input {...this.getInputProps("model")} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row type="flex" justify="space-between">
                        <Col span={11}>
                            <Form.Item label={getIntlMessage("data.condition")}>
                                <Select {...this.getProps("condition")}>
                                    { Object.values(ConditionEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key.toUpperCase()}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={11}>
                            <Form.Item label={getIntlMessage("data.color")}>
                                <ColorPicker
                                    {...this.getColorPickerProps("color")}
                                    placement="topLeft"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <h3>{getIntlMessage("segment.basic")}</h3>
                    <Row type="flex" justify="space-between">
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.basic.year")}>
                                <InputNumber
                                    {...this.getProps("basic.year")}
                                    min={0}
                                    max={new Date().getFullYear()}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.basic.price")}>
                                <InputNumber
                                    {...this.getProps("basic.price")}
                                    min={1}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.basic.mileage")}>
                                <InputNumber
                                    {...this.getProps("basic.mileage")}
                                    min={0}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <h3>{getIntlMessage("segment.shape")}</h3>
                    <Row type="flex" justify="space-between">
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.shape.body")}>
                                <Select {...this.getProps("shape.body")}>
                                    {Object.values(BodyStyleEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key.toUpperCase()}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.shape.doors")}>
                                <InputNumber
                                    {...this.getProps("shape.doors")}
                                    min={1}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.shape.seats")}>
                                <InputNumber
                                    {...this.getProps("shape.seats")}
                                    min={1}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <h3>{getIntlMessage("segment.engine")}</h3>
                    <Row type="flex">
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.engine.fuel")}>
                                <Select {...this.getProps("engine.fuel")}>
                                    {Object.values(FuelEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key.toUpperCase()}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.engine.capacity")}>
                                <InputNumber
                                    {...this.getProps("engine.capacity")}
                                    min={0}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row type="flex">
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.engine.power.number")}>
                                <InputNumber
                                    {...this.getProps("engine.power.number")}
                                    min={0}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.engine.power.type")}>
                                <Select {...this.getProps("engine.power.type")}>
                                    {Object.values(PowerTypeEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={7}>
                            <Form.Item label={getIntlMessage("data.engine.transmission")}>
                                <Select {...this.getProps("engine.transmission")}>
                                    {Object.values(TransmissionEnum).map(key => (
                                        <Select.Option key={key} value={key}>{key.toUpperCase()}</Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <h3>{getIntlMessage("segment.features")}</h3>
                    <Row type="flex" justify="space-between">
                        <Col span={11}>
                            <Form.Item label={getIntlMessage("data.features.interior")}>
                                <Select
                                    mode="tags"
                                    {...this.getProps("features.interior")}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={11}>
                            <Form.Item label={getIntlMessage("data.features.exterior")}>
                                <Select
                                    mode="tags"
                                    {...this.getProps("features.exterior")}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Divider dashed />
                    <Pictures>
                        { selected && (
                            <ImageRow key={selected.pictures.main.Location}>
                                <Col span={2}>
                                    <Switch checked />
                                </Col>
                                <Col span={22}>
                                    <ImagePreview link={selected.pictures.main.Location} />
                                </Col>
                            </ImageRow>
                        ) }
                        { selected && selected.pictures.list.map(item => (
                            <ImageRow key={item.Location}>
                                <Col span={2}>
                                    <Switch checked={false} />
                                </Col>
                                <Col span={22}>
                                    <ImagePreview link={item.Location} />
                                </Col>
                            </ImageRow>
                        )) }
                        { this.state.files.map((file, index) => (
                            <ImageRow key={file.uid}>
                                <Col span={2}>
                                    <Switch checked={!!file.main} onChange={this.handleMainImageChange(file.uid)} />
                                    <Button icon="close" onClick={this.handleRemoveImageClick(file.uid)} />
                                </Col>
                                <Col span={22}>
                                    <ImagePreview image={file} />
                                </Col>
                                {index < 2 && index !== this.state.files.length - 1 && (<Divider />)}
                            </ImageRow>
                        )) }
                        { !selected && this.state.files.length < VEHICLE_PICTURES_LIST_LIMIT && (
                            <Upload.Dragger {...this.getUploadProps()}>
                                <p className="ant-upload-drag-icon">
                                    <Icon type="inbox" />
                                </p>
                                <p className="ant-upload-text">
                                    { getIntlMessage("uielement.upload.text") }
                                </p>
                            </Upload.Dragger>
                        )}
                    </Pictures>
                    <Divider />
                    <Row type="flex" justify="space-between">
                        <Button
                            icon="arrow-left"
                            onClick={() => this.props.history.push("/vehicles")}
                        />
                        <Button
                            type="primary"
                            icon="save"
                            loading={this.props.vehicles.fetching}
                            onClick={this.handleSubmit}
                            disabled={!formValid}
                        >
                            {getIntlMessage("action.submit")}
                        </Button>
                    </Row>
                </LayoutContent>
            </Container>
        )
    }
    isFormChanged = (selected = this.state.data) => !isEqual(
        this.state.data,
        this.getTransformed(selected),
    )
    handleMainImageChange = uid => value => this.setState(s => ({
        files: s.files.map((fileitem) => {
            const file = fileitem
            if (file.uid === uid) {
                file.main = value
            } else {
                file.main = !value
            }
            return file
        }),
    }))
    handleRemoveImageClick = uid => () => this.setState(s => ({
        files: s.files.filter(file => file.uid !== uid),
    }))
    getUploadProps = () => ({
        accept: "image/*",
        listType: "picture",
        fileList: this.state.files,
        multiple: true,
        showUploadList: false,
        beforeUpload: () => false,
        onChange: ({ fileList }) => {
            const files = fileList.map((fileitem) => {
                const file = fileitem
                if (fileList.length === 1 && !file.main) {
                    file.main = true
                }
                return file
            })
            this.setState({ files })
        },
    })
    getInitOptions = (data, updateState = true) => ({
        Entity: VehicleModel,
        updateState,
        entityValidationOptions: {
            groups: data ? ["update"] : ["create"],
        },
    })

    handleSubmit = () => {
        const { selected } = this.props.vehicles
        if (!selected) {
            this.props.create({ data: this.state.data, files: this.state.files })
        } else {
            this.props.update({ uuid: selected.uuid, data: this.state.data })
        }
    }
}

export default connect(
    (state, router) => ({
        vehicles: {
            selected: state.vehicles.data.find(item => item.uuid === router.match.params.id),
            fetching: state.vehicles.fetching,
        },
    }),
    {
        create: aVehicles.requestCreate,
        update: aVehicles.requestUpdate,
        get: aVehicles.requestGet,
    }
)(VehicleForm)
