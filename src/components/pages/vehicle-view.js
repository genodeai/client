import React from "react"
import { connect } from "react-redux"
import { get } from "lodash"

import { LayoutContent } from "components/commons/wrappers"
import { Row } from "components/commons/grid"
import { Tabs, List, ColorPicker } from "components/commons/uielements"
import { IntlMessage } from "components/commons/utility"
import { setDocTitle, chunkArray } from "helpers/utility"
import aVehicles from "store/vehicles/actions"

import { Container, General, DetailsListItem } from "./style/vehicle-view.style"

const getIntlMessage = (path = "") => (
    <IntlMessage id={`page.vehicle-view.${path}`} />
)

const CHUNK_SIZE = 5

class VehicleView extends React.Component {
    state = {
        preview: null,
    }
    componentDidMount() {
        setDocTitle("asfdsgds")
        this.props.get(this.props.match.params.id)
    }
    render() {
        const { selected } = this.props
        return (
            <Container>
                <LayoutContent actions={[{
                    icon: "arrow-left",
                    handler: () => this.props.history.push("/vehicles"),
                }]}
                >
                    { selected
                        ? (
                            <div>
                                <General>
                                    <General.Pictures>
                                        <General.Pictures.Main>
                                            <img src={this.state.preview || selected.pictures.main.Location} />
                                        </General.Pictures.Main>
                                        <General.Pictures.List>
                                            {chunkArray(selected.pictures.list, CHUNK_SIZE).map((row, rowIndex) => (
                                                <Row key={String(rowIndex)}>
                                                    {row.map(col => (
                                                        <img
                                                            src={col.Location}
                                                            onMouseEnter={() => this.setState({ preview: col.Location })}
                                                            onMouseLeave={() => this.setState({ preview: null })}
                                                        />
                                                    ))}
                                                </Row>
                                            ))}
                                        </General.Pictures.List>
                                    </General.Pictures>
                                    <General.Description>
                                        <h2>{selected.brand}</h2>
                                        <h3>{selected.model}</h3>
                                        <h3>{selected.basic.price}</h3>
                                    </General.Description>
                                </General>
                                <Tabs defaultActiveKey="0">
                                    <Tabs.TabPane tab={getIntlMessage("tabs.details")} key="0">
                                        <List
                                            size="small"
                                            dataSource={this.getDetailsListDataset()}
                                            renderItem={item => (
                                                <List.Item>
                                                    <DetailsListItem.Title>
                                                        {getIntlMessage(item.titleId)}
                                                    </DetailsListItem.Title>
                                                    <DetailsListItem.Value>
                                                        {!item.selector
                                                            ? item.select()
                                                            : get(selected, item.selector)
                                                        }
                                                    </DetailsListItem.Value>
                                                </List.Item>
                                            )}
                                        />
                                    </Tabs.TabPane>
                                    <Tabs.TabPane tab={getIntlMessage("tabs.features")} key="1">
                                        {this.getFeaturesList(selected.features.interior)}
                                        {this.getFeaturesList(selected.features.exterior)}
                                    </Tabs.TabPane>
                                </Tabs>
                            </div>
                        ) : (
                            <div>No content</div>
                        )
                    }
                </LayoutContent>
            </Container>
        )
    }
    getFeaturesList = dataset => (
        <ul>
            { dataset.map(item => (
                <li key={item}>{item}</li>
            )) }
        </ul>
    )
    getDetailsListDataset = (d = this.props.selected) => [
        {
            titleId: "details.year",
            selector: "basic.year",
        }, {
            titleId: "details.fuel",
            selector: "engine.fuel",
        }, {
            titleId: "details.capacity",
            selector: "engine.capacity",
        }, {
            titleId: "details.body",
            selector: "shape.body",
        }, {
            titleId: "details.power",
            select: () => (<span>{`${d.engine.power.number}${d.engine.power.type}`}</span>),
        }, {
            titleId: "details.color",
            select: () => <ColorPicker color={d.color} disableAlpha />,
        }, {
            titleId: "details.seats",
            selector: "shape.seats",
        }, {
            titleId: "details.mileage",
            selector: "basic.mileage",
        },
    ]
}

export default connect(
    (state, router) => ({
        selected: state.vehicles.data.find(item => item.uuid === router.match.params.id),
    }),
    {
        get: aVehicles.requestGet,
    }
)(VehicleView)
