import React from "react"
import { connect } from "react-redux"
import { get } from "lodash"

import { LayoutContent } from "components/commons/wrappers"
import { Row, Col } from "components/commons/grid"
import { Card, Icon } from "components/commons/uielements"
import { IntlMessage } from "components/commons/utility"
import { setDocTitle, chunkArray } from "helpers/utility"
import aVehicles from "store/vehicles/actions"

import { Container, PlusVehicle, CardDescription, CardCover } from "./style/vehicles.style"

const getIntlMessage = (path = "") => (
    <IntlMessage id={`page.vehicles.${path}`} />
)

const CHUNK_SIZE = 3

const details = [{
    titleId: "info.year",
    selector: "basic.year",
}, {
    titleId: "info.capacity",
    selector: "engine.capacity",
}, {
    titleId: "info.fuel",
    selector: "engine.fuel",
}, {
    titleId: "info.transmission",
    selector: "engine.transmission",
}]

class Vehicles extends React.Component {
    componentDidMount() {
        setDocTitle("Vehicles")
        this.props.list()
    }
    render() {
        const data = [{ plus: true }].concat(this.props.vehicles.data)
        return (
            <Container>
                <LayoutContent>
                    { chunkArray(data, CHUNK_SIZE).map((row, rowIndex) => (
                        <Row key={String(rowIndex)}>
                            { row.map((col) => {
                                let content = null
                                if (col.plus) {
                                    content = (
                                        <PlusVehicle
                                            hoverable
                                            cover={<Icon type="plus" />}
                                            onClick={() => this.props.history.push("/vehicles/new")}
                                        />
                                    )
                                } else {
                                    content = (
                                        <Card
                                            hoverable
                                            cover={<CardCover
                                                src={col.pictures.main.Location}
                                                onClick={() => this.props.history.push(`/vehicles/${col.uuid}/view`)}
                                            />}
                                            actions={[
                                                (
                                                    <Icon
                                                        type="edit"
                                                        onClick={() => this.props.history.push(`/vehicles/${col.uuid}/edit`)}
                                                    />
                                                ), (
                                                    <Icon
                                                        type="delete"
                                                    />
                                                ),
                                            ]}
                                        >
                                            <Card.Meta
                                                title={(<span>{`${col.brand} ${col.model}`}</span>)}
                                                description={(
                                                    <CardDescription>
                                                        <CardDescription.Info>
                                                            { details.map(item => (
                                                                <p key={item.titleId}>
                                                                    <span>{getIntlMessage(item.titleId)}:</span>
                                                                    <span>{get(col, item.selector)}</span>
                                                                </p>
                                                            )) }
                                                        </CardDescription.Info>
                                                        <CardDescription.Price>
                                                            {col.basic.price}
                                                        </CardDescription.Price>
                                                    </CardDescription>
                                                )}
                                            />
                                        </Card>
                                    )
                                }
                                return (<Col span={7} key={String(col.uuid)}>{content}</Col>)
                            })}
                        </Row>
                    )) }
                </LayoutContent>
            </Container>
        )
    }
}

export default connect(
    state => ({
        vehicles: state.vehicles,
    }),
    {
        list: aVehicles.requestList,
    }
)(Vehicles)
