import React from "react"
import { Route, Redirect, Switch } from "react-router-dom"
import { ConnectedRouter } from "react-router-redux"
import { connect } from "react-redux"

import asyncComponent from "helpers/AsyncFunc"


// const RestrictedRoutes = ({ authenticated, authenticating }) => {
//     if (!authenticated || authenticating) {
//         return null
//     }
//     return null
//     // return (
//     //     <Switch>
//     //         <Route
//     //             exact
//     //             path="/dashboard/"
//     //             component={asyncComponent(() => import("./pages/merchant/merchant-form"))}
//     //         />
//     //     </Switch>
//     // )
// }
const PublicRoutes = ({ history }) => (
    <ConnectedRouter history={history}>
        <Switch>
            <Route
                exact
                path="/vehicles"
                component={asyncComponent(() => import("./pages/vehicles"))}
            />
            <Route
                exact
                path="/vehicles/new"
                component={asyncComponent(() => import("./pages/vehicle-form"))}
            />
            <Route
                exact
                path="/vehicles/:id/edit"
                component={asyncComponent(() => import("./pages/vehicle-form"))}
            />
            <Route
                exact
                path="/vehicles/:id/view"
                component={asyncComponent(() => import("./pages/vehicle-view"))}
            />

            {/* <RestrictedRoutes
                authenticated={authenticated}
                authenticating={authenticating}
            /> */}

            <Route path="*" component={() => (<Redirect to="/vehicles" />)} />
        </Switch>
    </ConnectedRouter>
)

export default connect(({ auth }) => ({
    authenticating: auth.fetching,
    authenticated: auth.authenticated,
}))(PublicRoutes)
