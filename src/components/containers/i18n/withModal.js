import React from "react"
import { connect } from "react-redux"
import Modal from "components/commons/feedback/modal"
import Button from "components/commons/uielements/button"
import actions from "store/languageSwitcher/actions"
import config from "./config"

const LanguageSwitcher = (props) => {
    const {
        isActivated,
        language,
        switchActivation,
        changeLanguage
    } = props
    return (
        <div className="isoButtonWrapper">
            <Button type="primary" className="" onClick={switchActivation}>
          Switch Language
            </Button>

            <Modal
                title="Select Language"
                visible={isActivated}
                onCancel={switchActivation}
                cancelText="Cancel"
                footer={[]}
            >
                <div>
                    {config.options.map((option) => {
                        const { languageId, text } = option
                        const type =
                languageId === language.languageId ? "primary" : "success"
                        return (
                            <Button
                                type={type}
                                key={languageId}
                                onClick={() => {
                                    changeLanguage(languageId)
                                }}
                            >
                                {text}
                            </Button>
                        )
                    })}
                </div>
            </Modal>
        </div>
    )
}

export default connect(
    state => ({
        ...state.LanguageSwitcher.toJS()
    }),
    {
        switchActivation: actions.switchActivation,
        changeLanguage: actions.changeLanguage
    }
)(LanguageSwitcher)
