import styled from "styled-components"
import { Layout as AntLayout } from "antd"

import { palette } from "styles/constants"

export const Container = styled.div`
	.trigger {
	  	font-size: 18px;
	  	line-height: 64px;
	  	padding: 0 16px;
	  	cursor: pointer;
	  	transition: color 0.3s;
	  	&:hover {
			color: ${palette.primary[0]};
	  	}
	}
	
	.ant-layout-sider-collapsed {
		.anticon {
			font-size: 16px;
		}
		.nav-text {
			display: none;
		}
  	}

	.ant-layout-footer {
		font-size: 13px;
		@media (max-width: 767px) {
		  padding: 10px 20px;
		}
    }
	
	button {
		border-radius: 0;
	}
`

export const Layout = styled(AntLayout)`
    background: ${palette.secondary[1]};
    overflow: auto;
    overflow-x: hidden;
    @media only screen and (min-width: 768px) and (max-width: 1220px) {
        width: calc(100% - 64px);
        flex-shrink: 0;
    }

    @media only screen and (max-width: 767px) {
        width: 100%;
        flex-shrink: 0;
    }
`

export const Content = styled(AntLayout.Content)`
    padding: 70px 0 0;
    flex-shrink: 0;
    background: #f1f3f6;
`
