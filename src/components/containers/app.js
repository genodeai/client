import React from "react"
import { connect } from "react-redux"
import { Debounce } from "react-throttle"
import WindowResizeListener from "react-window-size-listener"

import { setDocTitle } from "helpers/utility"

import authAction from "store/auth/actions"
import appActions from "store/app/actions"
import { siteConfig } from "../../config"

// import Sidebar from "./sidebar"
import Topbar from "./topbar/topbar"

import { Container, Content, Layout } from "./style/app.style"

const { Footer } = Layout
const { logout } = authAction
const { toggleAll } = appActions
class App extends React.Component {
    componentDidMount() {
        setDocTitle("Dashboard")
    }
    render() {
        return (
            <Container>
                <Layout style={{ height: "100vh" }}>
                    <Debounce time="1000" handler="onResize">
                        <WindowResizeListener
                            onResize={windowSize =>
                                this.props.toggleAll(
                                    windowSize.windowWidth,
                                    windowSize.windowHeight
                                )}
                        />
                    </Debounce>
                    <Topbar />
                    <Layout style={{ flexDirection: "row", overflowX: "hidden" }}>
                        <Layout>
                            <Content>
                                { this.props.children }
                            </Content>
                            <Footer
                                style={{
                                    background: "#ffffff",
                                    textAlign: "center",
                                    borderTop: "1px solid #ededed",
                                }}
                            >
                                {siteConfig.footerText}
                            </Footer>
                        </Layout>
                    </Layout>
                </Layout>
            </Container>
        )
    }
}

export default connect(
    state => ({
        auth: state.auth,
    }),
    { logout, toggleAll }
)(App)
