import React from "react"
import { connect } from "react-redux"

import { Container, Header, SegmentRight } from "./style/topbar.style"


const Topbar = () => (
    <Container>
        <Header>
            <SegmentRight />
        </Header>
    </Container>
)

export default connect(state => ({
    ...state.app,
}))(Topbar)
