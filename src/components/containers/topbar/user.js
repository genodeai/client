import React, { Component } from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import PropTypes from "prop-types"

import Popover from "components/commons/uielements/popover"
import IntlMessages from "components/commons/utility/intlMessages"
import userpic from "styles/images/user1.png"
import actionAuth from "store/auth/actions"

import styles from "./styles/dropdown.module.scss"

class TopbarUser extends Component {
    static propTypes = {
        onClick: PropTypes.func,
    }
    static defaultProps = {
        onClick: () => {},
    }
    constructor(props) {
        super(props)
        this.handleVisibleChange = this.handleVisibleChange.bind(this)
        this.hide = this.hide.bind(this)
        this.state = {
            visible: false,
        }
    }
    render() {
        return (
            <li onClick={this.props.onClick}>
                <Popover
                    content={(
                        <div className={`${styles.container} ${styles.user}`}>
                            <a className={styles.link} onClick={this.props.logout}>
                                <IntlMessages id="topbar.logout" />
                            </a>
                        </div>
                    )}
                    trigger="click"
                    visible={this.state.visible}
                    onVisibleChange={this.handleVisibleChange}
                    arrowPointAtCenter
                    placement="bottomLeft"
                >
                    <div className={styles["img-wrapper"]}>
                        <img alt="user" src={userpic} />
                        <span className={styles["user-activity"]} />
                    </div>
                </Popover>
            </li>
        )
    }
    hide() {
        this.setState({ visible: false })
    }
    handleVisibleChange() {
        this.setState({ visible: !this.state.visible })
    }
}
export default connect(
    null,
    dispatch => ({
        logout: bindActionCreators(actionAuth.logout, dispatch),
    })
)(TopbarUser)
