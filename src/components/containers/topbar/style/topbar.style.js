import styled from "styled-components"
import { Layout } from "antd"

import { palette } from "styles/constants"
import themes from "styles/config/themes"
import { themeConfig } from "config"

export const Container = styled.div``

export const Header = styled(Layout.Header)`
    background: ${themes[themeConfig.theme].backgroundColor};
    position: fixed;
    width: 100%;
    height: 70px;
    display: flex;
    justify-content: flex-end;
    background-color: ${palette.grayscale[11]};
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    box-shadow: 0 1px 5px 0 rgba(41,85,115,.21);
    padding: 0 31px 0 265px;
    z-index: 1000;
    @include transition();

    @media only screen and (max-width: 767px) {
        padding: 0px 15px 0px 260px !important;
    }
`


export const SegmentLeft = styled.ul`
    display: flex;
    align-items: center;
    @media only screen and (max-width: 767px) {
        margin: 0 20px 0 0;
    }
`
export const SegmentRight = styled.ul`
    display: flex;
    align-items: flex-end;

    li {
        margin-left: 0;
        margin-right: 35px;
        cursor: pointer;
        line-height: normal;
        position: relative;
        display: inline-block;
        @media only screen and (max-width: 360px) {
            margin-left: 0;
            margin-right: 25px;
        }
        &:last-child {
            margin: 0;
        }
        i {
            font-size: 24px;
            color: ${palette.text[0]};
            line-height: 1;
        }
    }
`
