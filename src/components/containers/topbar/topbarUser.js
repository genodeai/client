import React, { Component } from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"

import Popover from "components/commons/uielements/popover"
import IntlMessages from "components/commons/utility/intlMessages"
import userpic from "styles/images/user1.png"
import actionAuth from "store/auth/actions"
import TopbarDropdownWrapper from "./topbarDropdown.style"

class TopbarUser extends Component {
    constructor(props) {
        super(props)
        this.handleVisibleChange = this.handleVisibleChange.bind(this)
        this.hide = this.hide.bind(this)
        this.state = {
            visible: false,
        }
    }

    render() {
        const content = (
            <TopbarDropdownWrapper className="isoUserDropdown">
                <a className="isoDropdownLink">
                    <IntlMessages id="themeSwitcher.settings" />
                </a>
                <a className="isoDropdownLink">
                    <IntlMessages id="sidebar.feedback" />
                </a>
                <a className="isoDropdownLink">
                    <IntlMessages id="topbar.help" />
                </a>
                <a className="isoDropdownLink" onClick={this.props.logout}>
                    <IntlMessages id="topbar.logout" />
                </a>
            </TopbarDropdownWrapper>
        )

        return (
            <Popover
                content={content}
                trigger="click"
                visible={this.state.visible}
                onVisibleChange={this.handleVisibleChange}
                arrowPointAtCenter
                placement="bottomLeft"
            >
                <div className="isoImgWrapper">
                    <img alt="user" src={userpic} />
                    <span className="userActivity online" />
                </div>
            </Popover>
        )
    }
    hide() {
        this.setState({ visible: false })
    }
    handleVisibleChange() {
        this.setState({ visible: !this.state.visible })
    }
}
export default connect(
    null,
    dispatch => ({
        logout: bindActionCreators(actionAuth.logout, dispatch),
    })
)(TopbarUser)
