import React from "react"
import { Provider } from "react-redux"
import { ThemeProvider } from "styled-components"
import AppLocale from "i18n"

import PublicRoutes from "components/router"
import { store, history } from "store"
import runBoot from "store/boot"
import themes from "styles/config/themes"
import { LocaleProvider } from "components/commons/utility"

import App from "./app"
import { themeConfig } from "../../config"

const renderDashApp = () => (
    <ThemeProvider theme={themes[themeConfig.theme]}>
        <Provider store={store}>
            <LocaleProvider locale={store.getState().app.locale}>
                <App>
                    <PublicRoutes history={history} />
                </App>
            </LocaleProvider>
        </Provider>
    </ThemeProvider>
)
runBoot()
    .then(() => renderDashApp())
    .catch(error => console.error(error))

export default renderDashApp
export { AppLocale }
