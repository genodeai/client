import React from "react"
import { connect } from "react-redux"
import { clone } from "lodash/fp"
import { Link } from "react-router-dom"
import { Layout } from "antd"
import { Scrollbars } from "react-custom-scrollbars"
import classNames from "classnames"

import { Menu, Dropdown, Icon } from "components/commons/uielements"
import { SIDEBAR_LEVELS } from "helpers/constants"
import aApp from "store/app/actions"
import aLevel from "store/level/actions"
import { Logo, IntlMessages } from "components/commons/utility"
import { CSSTransitionGroup } from "react-transition-group"
import styles from "./styles/sidebar.module.scss"

const { Sider } = Layout

class Sidebar extends React.Component {
    componentDidMount() {
        const items = this.getItems()

        const findCurrent = dataset => dataset.some((item) => {
            if (item.link === this.props.router.pathname) {
                this.props.changeCurrent(item.link)
                return true
            }
            return false
        })
        if (findCurrent(items) === false) this.props.changeCurrent(items[0].link, true)
    }
    render() {
        const { app, toggleOpenDrawer } = this.props
        const { openDrawer } = app
        const collapsed = clone(app.collapsed) && !clone(openDrawer)
        const merchant = this.props.merchant.data
        const { list } = this.props.location
        const { selected } = this.props.level

        const currentSelected = [merchant, ...list].find(item => item.storeId === selected)
        return (
            <div className={styles.container}>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={collapsed}
                    width="240"
                    className={styles.sidebar}
                    onMouseEnter={() => openDrawer === false && collapsed && toggleOpenDrawer()}
                    onMouseLeave={() => openDrawer === true && !collapsed && toggleOpenDrawer()}
                >
                    <Logo collapsed={collapsed} />
                    <Scrollbars
                        renderView={this.renderView}
                        style={{ height: app.height - 69 }}
                    >
                        { currentSelected !== undefined && this.props.user.merchantId && collapsed === false && (
                            <Dropdown
                                trigger={["click"]}
                                overlay={(
                                    <Menu
                                        className={styles.menu}
                                        onClick={this.handleLocationSelect}
                                    >
                                        <Menu.Item key={merchant.storeId} isMerchant>
                                            {merchant.title}
                                        </Menu.Item>
                                        <Menu.ItemGroup>
                                            { list.map(item => (
                                                <Menu.Item key={item.storeId} isMerchant={false}>
                                                    {item.title}
                                                </Menu.Item>
                                            )) }
                                        </Menu.ItemGroup>
                                    </Menu>
                                )}
                            >
                                <div className={styles["level-selector"]}>
                                    <a>
                                        <span>
                                            { currentSelected.title }
                                        </span>
                                        <Icon type="down" />
                                    </a>
                                </div>
                            </Dropdown>
                        ) }
                        { currentSelected && this.props.user.merchantId && collapsed === true && (
                            <div className={styles["level-selector"]}>
                                <a>
                                    <span>{ currentSelected.title }</span>
                                </a>
                            </div>
                        ) }
                        <CSSTransitionGroup
                            transitionName={this.props.level.isMerchant ? "slide" : "slide-rev"}
                            transitionEnterTimeout={350}
                            transitionLeaveTimeout={350}
                        >
                            <div
                                key={this.props.level.isMerchant ? "slide" : "slide-rev"}
                                className={styles["menu-group"]}
                            >
                                <Menu
                                    onClick={this.handleMenuItem}
                                    theme="dark"
                                    mode={collapsed === true ? "vertical" : "inline"}
                                    selectedKeys={[app.current]}
                                    className={styles.menu}
                                >
                                    { this.renderLevelItems(collapsed) }
                                </Menu>
                            </div>
                        </CSSTransitionGroup>
                    </Scrollbars>
                    <Icon
                        type="to-top"
                        className={classNames({
                            [styles.toggle]: true,
                            [styles.collapsed]: app.collapsed,
                        })}
                        onClick={this.props.toggleCollapsed}
                    />
                </Sider>
            </div>
        )
    }
    handleMenuItem = ({ key, ...args }) => {
        const { parentLink } = args.item.props

        this.props.changeCurrent(parentLink || key)

        if (this.props.app.view === "MobileView") {
            setTimeout(() => {
                this.props.toggleCollapsed()
                this.props.toggleOpenDrawer()
            }, 100)
        }
    }

    handleLocationSelect = (value) => {
        this.props.select(
            value.key,
            value.item.props.isMerchant,
            this.props.router.pathname
        )
        this.props.changeCurrent("/dashboard/general", true)
    }

    renderView({ style, ...props }) {
        const viewStyle = {
            marginRight: "-17px",
            paddingRight: "9px",
            marginLeft: "0",
            paddingLeft: "0",
        }
        return (
            <div className="box" style={{ ...style, ...viewStyle }} {...props} />
        )
    }
    renderLevelItems = (collapsed) => {
        const currentItems = this.getItems()
        const { isMerchant } = this.props.level
        const elements = []

        if (isMerchant === false) {
            const item = SIDEBAR_LEVELS.MERCHANT.find(menuItem => menuItem.link === this.props.level.parentUrl)
                || SIDEBAR_LEVELS.MERCHANT[0]

            const { storeId } = this.props.merchant.data

            elements.push((
                <Menu.Item
                    key="back-btn"
                    parentLink={item.link}
                >
                    <Link
                        to={item.link}
                        onClick={() => this.props.select(storeId, true)}
                    >
                        <span className={styles["menu-holder"]}>
                            <Icon type="arrow-left" />
                            {collapsed === false && (<IntlMessages id="sidebar.back" />)}
                        </span>
                    </Link>
                </Menu.Item>
            ))
        }
        return elements.concat(currentItems.map((item) => {
            if (item.permission && !this.props.user.permissions.includes(item.permission)) {
                return null
            }
            return (
                <Menu.Item key={item.link}>
                    <Link to={item.link}>
                        <span className={styles["menu-holder"]}>
                            <Icon className={styles["menu-icon"]} type={item.icon || "file"} />
                            {collapsed === false && (<IntlMessages id={item.label} />)}
                            { "items" in item && collapsed === false && (
                                <Icon className={styles["submenu-arrow"]} type="right" />
                            )}
                        </span>
                    </Link>
                </Menu.Item>
            )
        }))
    }
    getItems = () => {
        if (this.props.level.isMerchant === false) {
            return SIDEBAR_LEVELS.LOCATION
        }
        return SIDEBAR_LEVELS.MERCHANT
    }
}

export default connect(
    state => ({
        app: state.app,
        router: {
            pathname: state.router.location.pathname,
        },
        user: {
            merchantId: state.user.info.data.merchantId,
            permissions: state.user.permissions.data,
        },
        level: {
            selected: state.level.selected,
            isMerchant: state.level.isMerchant,
            parentUrl: state.level.parentUrl,
        },
        merchant: {
            data: state.merchant.data,
        },
        location: {
            list: state.location.list,
            empty: state.location.list.length === 0,
        },
    }),
    {
        select: aLevel.select,
        toggleOpenDrawer: aApp.toggleOpenDrawer,
        toggleCollapsed: aApp.toggleCollapsed,
        changeCurrent: aApp.changeCurrent,
    }
)(Sidebar)
