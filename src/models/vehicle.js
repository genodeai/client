import { get } from "lodash"
import {
    LocationModel,
    BasicModel,
    ShapeModel,
    PowerModel,
    EngineModel,
    FeaturesModel,
    PicturesModel,
    VehicleModel,
} from "platform-server/dist/vehicle/model"
import {
    BodyStyleEnum,
    PowerTypeEnum,
    FuelEnum,
    TransmissionEnum,
    ConditionEnum,
} from "platform-server/dist/vehicle/interface"


LocationModel.prototype.init = function (options = {}) {
    this.name = get(options, "name", "")
    this.lat = get(options, "lat", 0)
    this.lng = get(options, "lng", 0)
    return this
}

BasicModel.prototype.init = function (options = {}) {
    this.year = get(options, "year", 2000)
    this.mileage = get(options, "mileage", 0)
    this.price = get(options, "price", 1)
    return this
}

ShapeModel.prototype.init = function (options = {}) {
    this.body = get(options, "body", BodyStyleEnum.ESTATE)
    this.doors = get(options, "doors", 1)
    this.seats = get(options, "seats", 1)
    return this
}

PowerModel.prototype.init = function (options = {}) {
    this.number = get(options, "number", 100)
    this.type = get(options, "type", PowerTypeEnum.PS)
    return this
}

EngineModel.prototype.init = function (options = {}) {
    this.fuel = get(options, "fuel", FuelEnum.PETROL)
    this.transmission = get(options, "transmission", TransmissionEnum.MANUAL)
    this.power = new PowerModel().init(options.power)
    this.capacity = get(options, "capacity", 0)
    return this
}

FeaturesModel.prototype.init = function (options = {}) {
    this.interior = get(options, "interior", [])
    this.exterior = get(options, "exterior", [])
    return this
}

PicturesModel.prototype.init = function (options = {}) {
    this.main = get(options, "main", "")
    this.list = get(options, "list", [])
    return this
}

VehicleModel.prototype.init = function (options = {}) {
    this.condition = get(options, "condition", ConditionEnum.NEW)
    this.brand = get(options, "brand", "")
    this.model = get(options, "model", "")
    this.color = get(options, "color", "#36c")
    this.location = new LocationModel().init(options.location)
    this.basic = new BasicModel().init(options.basic)
    this.shape = new ShapeModel().init(options.shape)
    this.engine = new EngineModel().init(options.engine)
    this.features = new FeaturesModel().init(options.features)
    this.pictures = new PicturesModel().init(options.pictures)
    return this
}

export {
    VehicleModel,
}
