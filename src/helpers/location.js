export const getDefaults = list =>
    list.filter(item => item.type === "default")

export const getStores = list =>
    list.filter(item => item.type === "store")
