const TOKEN = "token"

export default class Token {
    static set(token, remember = false) {
        if (remember === true) {
            localStorage.setItem(TOKEN, token)
        } else {
            sessionStorage.setItem(TOKEN, token)
        }
    }
    static get() {
        const local = localStorage.getItem(TOKEN)
        if (local) {
            return { token: local, remember: true }
        }
        const session = sessionStorage.getItem(TOKEN)
        if (session) {
            return { token: session, remember: false }
        }
        return null
    }
    static clear() {
        localStorage.removeItem(TOKEN)
        sessionStorage.removeItem(TOKEN)
    }
}
