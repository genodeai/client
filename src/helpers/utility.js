import { siteConfig } from "../config"

export function setDocTitle(page = "") {
    document.title = `${siteConfig.siteName} | ${page}`
}

export function clearToken() {
    localStorage.removeItem("id_token")
}

export function isProductionEnv() {
    return process.env.NODE_ENV === "production"
}

export function positiveIntegerParser(val) {
    const onlyPositiveIntegers = /^[0-9]+$/
    if (onlyPositiveIntegers.test(val) === true) return val
    return 0
}

export const createAction = type => (payload = {}) => ({
    type, payload,
})

export const chunkArray = (array, size) => {
    let index = 0
    const arrayLength = array.length
    const tempArray = []

    for (index = 0; index < arrayLength; index += size) {
        const chunk = array.slice(index, index + size)
        // Do something if you want with the group
        tempArray.push(chunk)
    }

    return tempArray
}
