import axios from "axios"
import { resolve } from "url"

// import Token from "helpers/token"

const instance = axios.create({
    baseURL: process.env.API_URL,
    timeout: 30000,
})

// instance.interceptors.response.use((res) => {
//     const oldTokenObj = Token.get()
//     if (oldTokenObj !== null) {
//         const newToken = res.headers["x-access-token"]
//         if (newToken !== undefined && oldTokenObj.token !== newToken) {
//             Token.set(newToken, oldTokenObj.remember)
//         }
//     }
//     return res
// }, err => Promise.reject(err))


// export default function request(method, url, data = {}, isProtected = true) {
//     const headers = {
//         "accept-language": "en-US",
//     }
//     if (isProtected === true) {
//         headers["x-access-token"] = Token.get().token
//     }
//     return instance({
//         method, url, data, headers,
//     })
// }

export const getApiURL = (postfix = "") => resolve(process.env.API_URL, postfix)

export default instance
