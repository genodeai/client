const siteConfig = {
    siteName: "Genodeai Portal",
    siteIcon: "ion-flash",
    footerText: `Genodeai ©${new Date().getFullYear()}`,
}
const themeConfig = {
    topbar: "themedefault",
    sidebar: "themedefault",
    layout: "themedefault",
    theme: "themedefault",
}
const language = "english"
export {
    siteConfig,
    language,
    themeConfig,
}
