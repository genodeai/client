import React from "react"
import ReactDOM from "react-dom"

import DashApp from "components/containers/dash-app"
import { isProductionEnv } from "helpers/utility"
import { registerObserver } from "react-perf-devtool"
import injectGlobal from "styles/global.style"

import "reflect-metadata"

import antd from "antd/dist/antd.css"

import { unregister as unregisterServiceWorker } from "./registerServiceWorker"

injectGlobal()
console.log(antd)

ReactDOM.render(<DashApp />, document.getElementById("root"))

// Hot Module Replacement API
if (module.hot) {
    // console.clear()
    module.hot.accept("components/containers/dash-app", () => {
        const NextApp = require("components/containers/dash-app").default
        ReactDOM.render(<NextApp />, document.getElementById("root"))
    })
}
unregisterServiceWorker()
if (isProductionEnv() === false) registerObserver()

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (isProductionEnv()) {
    require("offline-plugin/runtime").install() // eslint-disable-line global-require
}
