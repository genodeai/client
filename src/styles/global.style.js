import { injectGlobal } from "styled-components"

import { palette } from "./constants"

/* ********* Add Your Global CSS Here ********* */
export default () => injectGlobal`
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    a,
    p,
    li,
    input,
    textarea,
    span,
    div,
    html,
    body,
    html a {
        margin-bottom: 0;
        font-family: 'Roboto', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.004);
    }

    .ant-popover-placement-bottom > .ant-popover-content > .ant-popover-arrow:after,
    .ant-popover-placement-bottomLeft > .ant-popover-content > .ant-popover-arrow:after,
    .ant-popover-placement-bottomRight > .ant-popover-content > .ant-popover-arrow:after,
    .ant-popover-placement-top > .ant-popover-content > .ant-popover-arrow:after,
    .ant-popover-placement-topLeft > .ant-popover-content > .ant-popover-arrow:after,
    .ant-popover-placement-topRight > .ant-popover-content > .ant-popover-arrow:after {
        left: 0;
        margin-left: -4px;
    }

    input {
        &::-webkit-input-placeholder,
        &:-moz-placeholder,
        &::-moz-placeholder,
        &:-ms-input-placeholder {
            color: ${palette.grayscale[0]};
        }
    }

    &.ant-message {
        .ant-message-notice-content {
            padding: 12px 16px;
            border-radius: 4px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
            background: #fff;
            display: inline-block;
            pointer-events: all;

            .ant-message-custom-content {
                font-size: 13px;
                color: ${palette.text[1]};

                .anticon {
                    margin-right: 8px;
                    font-size: 15px;
                    top: 1px;
                    position: relative;
                }

                &.ant-message-error {
                    .anticon {
                        color: ${palette.error[0]};
                    }
                }
            }
        }
    }
`
